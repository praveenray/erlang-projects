%%%-------------------------------------------------------------------
%%% File    : tm_master.erl
%%% Author  : Praveen <praveen@lenovo>
%%% Description : 
%%%
%%% Created : 28 Jun 2009 by Praveen <praveen@lenovo>
%%%-------------------------------------------------------------------
-module(tm_master).

-behaviour(gen_server).

%% API
-export([start_link/0, start_of_process/0, start_ticker_database_workflow/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-record(state, {
}).

%%====================================================================
%% API
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start_link() -> {ok,Pid} | ignore | {error,Error}
%% Description: Starts the server
%%--------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%====================================================================
%% gen_server callbacks
%%====================================================================

%%--------------------------------------------------------------------
%% Function: init(Args) -> {ok, State} |
%%                         {ok, State, Timeout} |
%%                         ignore               |
%%                         {stop, Reason}
%% Description: Initiates the server
%%--------------------------------------------------------------------
init([]) ->
    State = #state{},
%    {ok, _} = timer:apply_after(5*60*1000, ?MODULE, start_of_process, []),    
    {ok, State}
.

start_of_process() ->
    {_, {H,M,_S}} = erlang:localtime(),
    {ok, Start_time} = application:get_env(ticker_master, start_time_in_mins),
    case ((H*60 + M) >= Start_time) of
        true ->
            log4erl:debug("Starting Master Process at ~p",[erlang:localtime()]),
            {ok, Interval}   = application:get_env(ticker_master, sleep_interval_in_sec),
            {ok, _} = timer:apply_interval(Interval*1000, ?MODULE, start_ticker_database_workflow , []),
            start_ticker_database_workflow();
        false ->
            {ok, _} = timer:apply_after(5*60*1000, ?MODULE, start_of_process, [])
    end
.

start_ticker_database_workflow() ->
    {ok, DownloadDataApp} = application:get_env(ticker_master, download_data_app),
    gen_server:cast(DownloadDataApp , {load_prices_incrementally, self()})
.


%%--------------------------------------------------------------------
%% Function: %% handle_call(Request, From, State) -> {reply, Reply, State} |
%%                                      {reply, Reply, State, Timeout} |
%%                                      {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, Reply, State} |
%%                                      {stop, Reason, State}
%% Description: Handling call messages
%%--------------------------------------------------------------------
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% Function: handle_cast(Msg, State) -> {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, State}
%% Description: Handling cast messages
%%--------------------------------------------------------------------
handle_cast({load_prices_done, {Y,M,D}, TickerCount}, State) ->
    {ok, Window_of_analysis} = application:get_env(ticker_master, price_volume_upswing_window),
    gen_server:cast(Analytics_server, {run_price_volume_upswings, {Y,M,D}, Window_of_analysis }),
    {noreply, State};

handle_cast({load_prices_failed, Reason}) ->
    log4erl:warn("Load Prices Failed: ~s",[Reason])

.

%%--------------------------------------------------------------------
%% Function: handle_info(Info, State) -> {noreply, State} |
%%                                       {noreply, State, Timeout} |
%%                                       {stop, Reason, State}
%% Description: Handling all non call/cast messages
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% Function: terminate(Reason, State) -> void()
%% Description: This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any necessary
%% cleaning up. When it returns, the gen_server terminates with Reason.
%% The return value is ignored.
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% Func: code_change(OldVsn, State, Extra) -> {ok, NewState}
%% Description: Convert process state when code is changed
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%%% Internal functions
%%--------------------------------------------------------------------
