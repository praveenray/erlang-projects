%%%-------------------------------------------------------------------
%%% File    : tm_sup.erl
%%% Author  : Praveen <praveen@lenovo>
%%% Description : 
%%%
%%% Created : 28 Jun 2009 by Praveen <praveen@lenovo>
%%%-------------------------------------------------------------------
-module(tm_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start_link() -> {ok,Pid} | ignore | {error,Error}
%% Description: Starts the supervisor
%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================
%%--------------------------------------------------------------------
%% Func: init(Args) -> {ok,  {SupFlags,  [ChildSpec]}} |
%%                     ignore                          |
%%                     {error, Reason}
%% Description: Whenever a supervisor is started using 
%% supervisor:start_link/[2,3], this function is called by the new process 
%% to find out about restart strategy, maximum restart frequency and child 
%% specifications.
%%--------------------------------------------------------------------
init([]) ->
    AChild = {'tm_master',{tm_master, start_link,[]},
              permanent,2000,worker,[tm_master]},
    {ok,{{one_for_one,5,10}, [AChild]}}.

%%====================================================================
%% Internal functions
%%====================================================================
