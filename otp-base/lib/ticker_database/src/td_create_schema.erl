-module(td_create_schema).
-include("../include/td_includes.hrl").
-export([load_tickers/0, load_prices/0, load_prices/2, clear_prices/0, 
         load_tickers_with_java/1, create_database/0, create_tables/0, drop_tables/0, length/1]).


load_tickers() ->
    load_tickers_with_java("/home/praveen/erlang-projects/amex-symbols.csv"),
    load_tickers_with_java("/home/praveen/erlang-projects/nasdaq-symbols.csv"),
    load_tickers_with_java("/home/praveen/erlang-projects/nyse-symbols.csv")
.

load_prices() ->    
    clear_prices(),
    {TodayY, TodayM, TodayD}  = date(),
    YahooUrl = lists:flatten(io_lib:fwrite(
                               "http://ichart.finance.yahoo.com/table.csv?s=TICKER&a=~.B&b=~.B&c=~.B&d=~.B&e=~.B&f=~.B&g=d&ignore=.csv",
                               [0, 1, 2009, TodayM-1, TodayD, TodayY])
                            ),
    load_prices(YahooUrl, mnesia:dirty_all_keys(ticker))
.
    
load_prices(_Url,[]) -> {ok};
load_prices(YahooUrl,  [Ticker|Tickers]) ->
    io:fwrite("Fetching ~s~n", [Ticker]),
    Url      = re:replace(YahooUrl, "TICKER", Ticker, [{return,list}]),
    
    Resp = ibrowse:send_req(Url, [], get, [], 
                            [{save_response_to_file, true}],
                            5000),
    case Resp of 
        {ok, "200", _Headers, {file, FileName}} -> 
            {csvserver,'csvparser@ve-ruby-projects'} ! {self(), FileName },
            receive
                {ok, Lines} -> 
                    file:delete(FileName),
                    [_Header|Rest] = Lines,
                    insert_prices(Ticker, Rest),
                    timer:apply_after(10*1000, ticker_database, load_prices, [YahooUrl, Tickers]);
                _ -> {error, "Error Loading Prices"}
            end;
        _ -> io:fwrite("~s Failed~n",[Ticker]),
                    timer:apply_after(10*1000, ticker_database, load_prices, [YahooUrl, Tickers])                        
    end
.
   

insert_prices(_Ticker,[]) -> {ok};
insert_prices(Ticker, [H|T]) ->
    [Date,Open,High,Low, _Close,Volume,AdjClose] = H,
    Rec = #price{name = Ticker,
                 date = lists:filter(fun(X) -> X /= $- end,  Date),
                 open = Open,
                 close = AdjClose,
                 high = High,
                 low  = Low,
                 id  = uuid:new(),
                 volume = Volume},
    mnesia:transaction(fun() -> mnesia:write(Rec) end),
    insert_prices(Ticker, T)
.

clear_prices() ->
    mnesia:clear_table(price).

load_tickers_with_java(CsvFile) ->
    {csvserver,'csvparser@ve-ruby-projects'} ! {self(), CsvFile},
    receive
        {ok, Lines} -> 
            io:fwrite("~p\n", [lists:foldl(fun(_,A) -> A+1 end, 0, Lines)]),
            insert_tickers(Lines);
        _ -> {error, "java csv_parser not running?"}
    end.

insert_tickers([]) -> {ok};
insert_tickers([H|T]) ->
    insert_ticker(H),
    insert_tickers(T).

insert_ticker([Company_name,Symbol]) ->
    % io:fwrite("Inserting: ~s~n", [Symbol]),
    Rec = #ticker{name = Company_name, symbol = Symbol},
    mnesia:transaction(fun() -> mnesia:write(Rec) end)
.
                               

length(List) ->
    lists:foldl(fun(_,A) -> A+1 end, 0, List).
                         
create_database() ->
    mnesia:create_schema(node()).

create_tables() ->
    mnesia:create_table(ticker, [{disc_copies, [node()]}, {attributes, record_info(fields, ticker)} ]),
    mnesia:create_table(price, [{disc_copies, [node()]},  {attributes, record_info(fields, price)} ])
.

drop_tables() ->
    mnesia:delete_table(ticker),
    mnesia:delete_table(price).
