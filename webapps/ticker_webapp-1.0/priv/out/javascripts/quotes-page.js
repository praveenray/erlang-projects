Ext.ns('Yft.GFinance.QuotesPage');
Yft.GFinance.QuotesPage = function (config) {
    Yft.GFinance.QuotesPage.superclass.constructor.call(this, config);
}
Ext.extend(Yft.GFinance.QuotesPage, FW.Pages.BaseObj);
CURRENT_PROTO = Yft.GFinance.QuotesPage.prototype;

CURRENT_PROTO.init = function() {
    this.grid = new Yft.GFinance.QuotesGrid({
        width: 500,
        height: 300,
        bodyStyle:'padding: 10px 10px 10px;',
        renderTo: 'quotes-grid-div',
        frame: true,
        listeners: this.lsnr('edit-clicked', this._on_add_new_ticker)
    });
}

CURRENT_PROTO._on_add_new_ticker=function(ticker_id) {
    this._close_ticker_form();

    var form = new Yft.GFinance.TickerForm({
        id:'ticker-form',
        height: (Ext.isEmpty(ticker_id) ? 200 : 225),
        ticker_id: ticker_id,
        listeners: this.lsnr('closed', this._close_ticker_form, 'saved', function() {
            this._close_ticker_form();
            this.grid.getStore().reload();
        })
    });

    this.ticker_form = new Ext.Window({
        modal: true,
        width: 475,
        height: 185,
        layout:'fit',
        title: (ticker_id ? 'Edit' : 'New') + ' Ticker Information ',
        frame: true,
        items: form
    });
    this.ticker_form.show();    
}

CURRENT_PROTO._close_ticker_form=function() {
    if(this.ticker_form) {
        this.ticker_form.destroy();
        this.ticker_form = null;
    }
}
