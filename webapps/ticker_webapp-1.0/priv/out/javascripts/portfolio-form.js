Ext.ns('Yft.GFinance.PortfolioForm');
Yft.GFinance.PortfolioForm=function(config) {
    FW.Widgets.IBaseObj.fw_init.call(this,config);
    if(config.portfolio_id) {
        config.edit_data_root = 'portfolio_data';
        config.edit_url = this.url('/portfolios',{params:{id:config.portfolio_id}});
    }
    Yft.GFinance.PortfolioForm.superclass.constructor.call(this,config);
}

Ext.extend(Yft.GFinance.PortfolioForm, FW.Widgets.form.FormPanel, FW.Widgets.IBaseObj);
CURRENT_PROTO=Yft.GFinance.PortfolioForm.prototype;
Ext.reg('yft.gfinance.portfolioform',Yft.GFinance.PortfolioForm);

CURRENT_PROTO.initComponent=function() {
    this.addEvents('closed','saved');
    var content = {
        frame: true,
        name_prefix: 'portfolio',
        defaults: {width: 275, allowBlank: false, xtype:'textfield'},
        labelWidth: 50,
        items: [
            {
                name: 'portfolio_name',
                maxLength: 30
            },
            {
                name:'seed_amount',
                xtype:'numberfield',
                maxValue: 50000,
                minValue: 1000
            },
            {
                name:'tickers',
                xtype:'textarea',
                maxLength: 256,
                emptyText: 'Ticker Price',
                height: 130
            }
        ],
        buttons: [
            {
                text: 'Save',
                enter_key: true,
                handler: this._on_save
            },
            {
                text:'Close',
                handler: this._on_close
            }
        ]
    };
    Yft.GFinance.PortfolioForm.superclass.initComponent.call(Ext.apply(this, content));
}

CURRENT_PROTO._on_save=function() {
    if(this.getForm().isValid()) {
        this.submit({
            url: this.portfolio_id ? this.edit_url :  this.url('/portfolios'),
            method: (this.portfolio_id ? 'put' : 'post'),
            success: function(r) {
                r=this.ajax_process_notice(r);
                this.fireEvent('saved', r.page_data.portfolio_id);
            }
        });
    }
}

CURRENT_PROTO._on_close=function() {
    this.fireEvent('closed');
}
