Ext.ns('Yft.GFinance.HomePage');
Yft.GFinance.HomePage = function (config) {
    Yft.GFinance.HomePage.superclass.constructor.call(this, config);
}
Ext.extend(Yft.GFinance.HomePage, FW.Pages.BaseObj);
CURRENT_PROTO = Yft.GFinance.HomePage.prototype;

CURRENT_PROTO.init = function() {
    this.xhr({
        url: this.url('/sessions'),
        success: function(r) {
            r=this.ajax_process_notice(r);
            if(r.success && r.page_data.google_auth_token) {
                this.grid = new Yft.GFinance.PortfolioGrid({
                    height: 500,
                    width: 700,
                    listeners: this.lsnr('add-new-portfolio', this._on_add_new_portfolio,
                                         'edit-clicked', this._on_add_new_portfolio
                    ),
                    renderTo: 'portfolios-grid-div'
                });                
            }
        }
    });
}

CURRENT_PROTO._on_add_new_portfolio=function(portfolio_id) {
    this._close_portfolio_form();
    var form = new Yft.GFinance.PortfolioForm({
        id:'portfolio-form',
        height: 225,
        portfolio_id: portfolio_id,
        listeners: this.lsnr('closed', this._close_portfolio_form, 'saved', function(portfolio_id) { 
            this._close_portfolio_form();
            this.grid.getStore().reload();
        })
    });
    this.portfolio_win = new Ext.Window({
        modal: true,
        height: 325,
        width: 425,
        layout:'fit',
        title: (portfolio_id ? 'Edit' : 'New') + ' Portfolio Information ',
        frame: true,
        items: form
    });
    this.portfolio_win.show();
}

CURRENT_PROTO._close_portfolio_form=function() {
    if(this.portfolio_win) {
        this.portfolio_win.destroy();
        this.portfolio_win = null;
    }
}
