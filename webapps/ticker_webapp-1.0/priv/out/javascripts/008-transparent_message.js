Ext.onReady(function() {
FW.Widgets.TransparentMessage = {
    OPACITY: .90,
    // id and class
    MSG: "humanized_msg",
    //MSG_TARGET: "humanized_msg_div",
    TOP_OFFSET: 50,
    FORMAT: new Ext.XTemplate("<p>{msg}</p>"),
    _msgFadeEffect: null,
    _currMsgContent: '',
    config: null,

    getMessageOpacity: function() {
        /* Returns the current message opacity. */
        var e = FW.Widgets.TransparentMessage.layer;//Ext.get(FW.Widgets.TransparentMessage.MSG);
        if(!e.isVisible())
            return 0.0;
        return (1.0 * e.getStyle("opacity"));
    },

    getScrollHeight: function () {
        /* Returns the y scroll height in all browsers. */
        var y;
        // all except Explorer
        if (self.pageYOffset)
            y = self.pageYOffset;
        else if (document.documentElement && document.documentElement.scrollTop)
            y = document.documentElement.scrollTop;
        else if (document.body) // all other Explorers
            y = document.body.scrollTop;
        return y;
    },

    _showMessage: function(message, config) {
        /* A private method which shows the transparent message. Doesn't handle concurrency. */
        config = config || {};
        var e = FW.Widgets.TransparentMessage.layer; 
	e.update(FW.Widgets.TransparentMessage.msg_tmpl.applyTemplate({msg: message}));
		
        if(config.is_error)
            e.replaceClass('humanized_msg','humanized_error_msg');
        else {
            e.replaceClass('humanized_error_msg','humanized_msg');
            var t = new Ext.util.DelayedTask(FW.Widgets.TransparentMessage.hideMessage, FW.Widgets.TransparentMessage, [message, config]);
            t.delay(1000);
        }

        if(config.font_size) {
            FW.Widgets.TransparentMessage.custom_font_size = e.getStyle('font-size');
            e.setStyle('font-size', config.font_size);
        }

	e.center();
        FW.Widgets.TransparentMessage._currMsgContent = message;

        var pos = e.getXY();
        pos[1]  = FW.Widgets.TransparentMessage.getScrollHeight() + FW.Widgets.TransparentMessage.TOP_OFFSET;
        e.setY(pos[1]);
	e.show();

        FW.Widgets.TransparentMessage.config = config;
    },
    show_msg: function() {
        FW.Widgets.TransparentMessage.showMessage.apply(FW.Widgets.TransparentMessage, arguments);
    },
    show_error:function() {
        FW.Widgets.TransparentMessage.showError.apply(FW.Widgets.TransparentMessage, arguments);
    },
    showMessage: function ( message, config ) {
        /* Shows the message: handles the cases where a second message is attempted to be displayed before the first is done. */
        var e = FW.Widgets.TransparentMessage.layer; //Ext.get(FW.Widgets.TransparentMessage.MSG);
        if(e.hasActiveFx()) {
            // If the same message is displayed while the current message is being
            // faded away, it doesn't need to be shown again. This handles the case
            // where the user takes an action to dismiss a message and inadvertantly
            // causes the message to be shown again.
            if (FW.Widgets.TransparentMessage._currMsgContent != message ) {
                FW.Widgets.TransparentMessage._queueFx(message, config);
            }
        }
        else{
            FW.Widgets.TransparentMessage._queueFx(message, config);
        }
    },

    showError: function(message, config) {
        config = config || {}
        config.is_error = 1;
        FW.Widgets.TransparentMessage.showMessage(message,config);
    },

    _queueFx: function(message, config) {
        var t = new Ext.util.DelayedTask(FW.Widgets.TransparentMessage._showMessage, FW.Widgets.TransparentMessage, [message, config]);
        t.delay(100);
    },

    hideMessage: function () {
        /* Hides the message via fade animation. It won't attempt to hide a message if it is currently being hidden. */
        if (FW.Widgets.TransparentMessage.getMessageOpacity() >= FW.Widgets.TransparentMessage.OPACITY) {
            //Ext.get(FW.Widgets.TransparentMessage.MSG).fadeOut({
            if(FW.Widgets.TransparentMessage.custom_font_size) {
                FW.Widgets.TransparentMessage.layer.setStyle('font-size',FW.Widgets.TransparentMessage.custom_font_size);
                FW.Widgets.TransparentMessage.custom_font_size = null;
            }
            FW.Widgets.TransparentMessage.layer.fadeOut({
                    duration: 0.5,
                    useDisplay:false,
                    callback: function() {
                        if(FW.Widgets.TransparentMessage.config && FW.Widgets.TransparentMessage.config.handler) {
                            var scope = FW.Widgets.TransparentMessage.config.scope;
                            var fn    = FW.Widgets.TransparentMessage.config.handler;
                            var args  = FW.Widgets.TransparentMessage.config.args;
                            if(scope)
                                fn.call(scope, args);
                            else
                                fn(args);
                            FW.Widgets.TransparentMessage.config = null;
                        }
                    }
            });
            
            FW.Widgets.TransparentMessage.layer.stopFx();
			FW.Widgets.TransparentMessage.layer.hide();
        }
    },
    hide_msg: function() {
        FW.Widgets.TransparentMessage.hideMessage.apply(FW.Widgets.TransparentMessage, arguments);
    },
    init: function() {
        FW.Widgets.TransparentMessage.FORMAT.compile();
        FW.Widgets.TransparentMessage.msg_tmpl = new Ext.XTemplate(
	    "<table width='100%' border='0' cellpadding='0' cellspacing='0'>",
	      "<tr>",
	        "<td><div class='banner-message-left'></div></td>",
	        "<td width='100%'><div id='humanized_msg_div' class='banner-message-center'>",
                  "<span style='color:white;width:50%;'>{msg}</span></div></td>",
	        "<td><div class='banner-message-right'></div></td>",
	      "</tr>",
	    "</table>"
        );
	FW.Widgets.TransparentMessage.msg_tmpl.compile();
	FW.Widgets.TransparentMessage.layer = new Ext.Layer({
			shadow: false,
			cls: FW.Widgets.TransparentMessage.MSG
	});

	Ext.each(["mousedown", "keydown"], function(i) {
            Ext.get(document.body).on(i, FW.Widgets.TransparentMessage.hideMessage);
        });
    }
};
    FW.Widgets.TransparentMessage.init();
});
