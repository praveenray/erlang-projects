Ext.ns('Yft.Overlay');
Yft.Overlay=function(config) {
    config = Ext.applyIf((config || {}), {
        id:'yft-overlay',
        z_index: 14999,
        background_color: '000',
        height: 500
    });
    this.overlay = Ext.get(config.id);
    if(!this.overlay) {
        var z = delete(config.z_index);
        var bg = delete(config.background_color);
        var ht = String(delete(config.height)).replace(/px$/,'');
        this.overlay = Ext.DomHelper.append(document.body, config, true);
        this.overlay.hide();
    }
}
CURRENT_PROTO=Yft.Overlay.prototype;
CURRENT_PROTO.show=function(config) {
    config = Ext.applyIf((config || {}), {
        duration: 0.2,
        endOpacity: 0.65
    });
    var w = Ext.lib.Dom.getViewWidth(true);
    var h = Ext.lib.Dom.getViewHeight(true);
    this.overlay.setStyle({
        width: w+'px',
        height:h+'px'
    });
    this.overlay.fadeIn(config);
    var el = this._get_top_win(config);
    if(el) {
        this.top_win_zindex = el.getStyle('z-index');
        console.log('current z index: ' + this.top_win_zindex);
        el.setStyle({'z-index': this.get_z_index()+1});
    }
}

CURRENT_PROTO.hide=function(config) {
    this.overlay.fadeOut({duration: 0.2});
    var el=this._get_top_win(config);
    if(el) {
        console.log('set the z_index to : ' + this.top_win_zindex);
        el.setStyle({'z-index': this.top_win_zindex});
    }
}

CURRENT_PROTO.get_z_index=function() {
    return Number(this.overlay.getStyle('z-index'));
}

CURRENT_PROTO._get_top_win=function(config) {
    if(!config)
        return;
    var el = null;
    if(config.top_win) {
        if(config.top_win.getEl)
            el = config.top_win.getEl();
        else
            el = Ext.get(config.top_win);
    }
    return el;
}
