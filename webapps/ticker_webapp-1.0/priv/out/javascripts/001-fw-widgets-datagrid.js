Ext.ns('FW.Widgets.DataGrid');
FW.Widgets.DataGrid=function(config) {
    this.addEvents('remote_data_arrived');
    this.reader_total_property = this.reader_total_property || 'total_rows';
    this.reader_root = this.reader_root || 'data';
    this.reader_id_property = this.reader_id_property || 'id';
    this.enableHdMenu = ('enableHdMenu' in config) ? config.enableHdMenu : false;

    var grid_config = {
        sm: config.sm || new Ext.grid.RowSelectionModel({singleSelect:true}),
        stripeRows:true,
        trackMouseOver: true
    };
    if(config.editable_grid)
        grid_config.clicksToEdit = 1;

    Ext.applyIf(config, grid_config);    
    if(!config.__editor_grid)
        FW.Widgets.DataGrid.superclass.constructor.call(this,config);
}
Ext.extend(FW.Widgets.DataGrid, Ext.grid.GridPanel);
Ext.reg('fw.widgets.datagrid', FW.Widgets.DataGrid);
CURRENT_PROTO=FW.Widgets.DataGrid.prototype;
/**
    Useful to load data_object from MemoryProxy. Not to be called for Remote Sources
    @params {Array} data_object: Array of hashes to load as per configured reader
*/
CURRENT_PROTO.load_data=function(data_object) {
    var data = {};
    data[this.reader_root] = data_object;
    data[this.reader_total_property] = data_object.length;
    this.getStore().loadData(data);
}
CURRENT_PROTO.get_selected_ids=function() {
    var sm = this.getSelectionModel();
    var records = sm.getSelections();
    var id_array = [];
    Ext.each(records, function(r) {
            id_array.push(r.id);
    });
    return id_array;
}
CURRENT_PROTO.initComponent=function() {
    this.grid_data = this.grid_data || [];
    // IF YOU ADD ANY NEW METHOD CALLS HERE, REMEMBER TO ADD THESE TO PROTOTYPE OF FW.WIDGETS.EDITORDATAGRID,
    // BELOW IN THE SAME FILE
    this._create_col_model();
    this._create_data_proxy();
    this._create_data_reader();
    this._create_data_store();
    this._create_paging_toolbar();
    this._create_view_config();
    if(!this.__editor_grid)
        FW.Widgets.DataGrid.superclass.initComponent.call(this);
}
CURRENT_PROTO._create_data_store=function() {
    var store_config = {
        reader: this.data_reader,
        proxy: this.data_proxy,
        remoteSort:true,
        listeners: {
            load: function(store) {
                var array_data = [];
                Ext.each(store.data.items, function(i) {array_data.push(i.json)}, this);
                this.fireEvent('remote_data_arrived', array_data);
                if(this.getGridEl())
                    this.getGridEl().unmask();
            },
            beforeload: function(store) {
                if(this.getGridEl())
                    this.getGridEl().mask('Loading...');
                return true;
            },
            scope:this
        }
    };
    this.store_class = this.store_class || 'Ext.data.Store';
    this.ds = eval("new "+this.store_class+"(store_config)");
    if(this.grid_data.length > 0)
        this.ds.load();
}

CURRENT_PROTO._create_col_model=function() {
    var col_model = this.col_model;
    var editable_grid = false;
    Ext.each(this.col_model, function(one_col) {
        one_col.type = ('type' in one_col) ? one_col.type : 'string';
        one_col.sortable = ('sortable' in one_col) ? one_col.sortable : true;
        one_col.dataIndex=('dataIndex' in one_col) ? one_col.dataIndex : one_col.header.underscore();
        if(!one_col.renderer) {
            if(one_col.type == 'date' || one_col.type == 'datetime') {
                one_col.renderer = Ext.util.Format.dateRenderer(
                        one_col.type == 'date' ? 'n/j/Y' : 'n/j/Y g:i A'
                );
            }
            else if(one_col.type == 'boolean')
                one_col.renderer =  function(c) { return c ? 'Yes':'No'};
        }
        
        if(one_col.renderer) {
            one_col.renderer = one_col.renderer.createDelegate(one_col.scope || this);
        }
        if(one_col.editor)
            editable_grid = true;
        if(one_col.type && one_col.type == 'datetime') {
            one_col.data_type = one_col.type; // we need to overwrite type to be date always
            one_col.type = 'date';
        }
    }, this);
    this.editable_grid = editable_grid;
    //console.log(this.col_model);
    if(this.add_checkbox_model) {
        var sm = new Ext.grid.CheckboxSelectionModel();
        col_model = [sm].merge(this.col_model);
        this.sm = sm;
    }
    if(this.add_row_numbers) {
        var new_cm = [
            {
                header: '#',
                width:20,
                renderer:function(c,m,r,ri,ci,store) {
                    var start = store.lastOptions.params.start+1;
                    return (start+ri);
                }
            }];
        new_cm.merge(col_model);
        col_model = new_cm;
    }
    this.cm = new Ext.grid.ColumnModel(col_model);
}

CURRENT_PROTO._create_data_reader=function() {
    var mappings = [];
    Ext.each(this.col_model, function(one_col) {
            var one_rec_def = {
                name:one_col.dataIndex,
                type: ('type' in one_col) ? one_col.type : 'string'
            };
            if(one_rec_def.type == 'date')
                one_rec_def.dateFormat = 'n/j/Y' + (one_col.data_type == 'datetime' ? ' G:i' :'');
            mappings.push(one_rec_def);
    });
    var data_reader = new Ext.data.JsonReader({
        totalProperty: this.reader_total_property,
        id:  this.reader_id_property ,
        root: this.reader_root
    }, mappings);
    this.data_reader = data_reader;
}

CURRENT_PROTO._create_data_proxy=function() {
    var proxy = null;
    if(this.remote_url) {
        proxy = new Ext.data.HttpProxy({url: this.remote_url,method:'GET'});
    } else {
        var grid_data = {};
        grid_data[this.reader_total_property] = this.grid_data.length;
        grid_data[this.reader_root] = this.grid_data;
        proxy = new Ext.data.MemoryProxy(grid_data);
    }
    this.data_proxy = proxy;
}

CURRENT_PROTO._create_paging_toolbar=function() {
    if(!this.bbar && Ext.value(this.page_size, 0) > 0) {
        this.item_name = this.item_name || '';
        var msg = this.item_name.length > 0 ? ('Displaying '+ this.item_name + ' {0} - {1} of {2}') : '';
        this.bbar = new Ext.PagingToolbar({
            pageSize: this.page_size,
            store: this.ds,
            displayInfo: true,
            displayMsg: msg,
            emptyMsg: 'No ' + this.item_name + ' to display'
        });
    }
}

CURRENT_PROTO._create_view_config=function() {
    var view = null;
    var view_config = Ext.apply({
        forceFit: true,
        autoFill:true
    }, (this.viewConfig || {}));
    if(view_config) {
        var view_class = view_config.view_class || 'Ext.grid.GridView';
        if(this.store_class == 'Ext.data.GroupingStore') {
            if(!view_config.groupTextTpl)
                view_config.groupTextTpl = '{text}';
            view_class = 'Ext.grid.GroupingView';
        }

        if(view_class == 'Ext.grid.GridView')
            view = new Ext.grid.GridView(view_config);
        else if(view_class == 'Ext.grid.GroupingView')
            view = new Ext.grid.GroupingView(view_config);
    }
    this.view = view;
    delete this.viewConfig;
}

// link_element must be an <a> element
CURRENT_PROTO.attach_download_link=function(link_element, download_url, params) {
    var headers      = [];
    var data_indexes = [];        
    params           = params || {};

    Ext.each(this.col_model, function(col_model) {
        if(col_model.header != '#') {
            headers.push(col_model.header);
            data_indexes.push(col_model.dataIndex);
        }
    }, this);
    params.headers = headers.join();
    params.data_indexes = data_indexes.join();
    params.csv = 1;

    return params;
//     download_url = page.url(download_url, {params: params});

//     console.log("Download URL: " + download_url);
//     link_element = Ext.get(link_element);
//     if(link_element)
//         link_element.dom.setAttribute('href', download_url);
//     return download_url;
}

CURRENT_PROTO.search_data_store = function(search_box, evt, deferred) {
    if(!deferred) {
        this.last_search_key_press = new Date();
        this.search_data_store.defer(1000,this,[search_box,evt,true]);
        return;
    }
    var now = new Date();
    if(now.getElapsed(this.last_search_key_press) < 1000) return;
    this.last_search_key_press = null;

    var val = search_box.getValue().trim();
    if(val.length > 0) {
        if(this.remote_url) {
            this.getStore().load({
                url:this.remote_url,
                params: {start:0, limit: this.page_size, filter_by: this.search_column(), filter_value: val}
            });
        } else
            this.getStore().filter( this.search_column() ,val,false,false);
    } else {
        this.getStore().clearFilter();
        this.getStore().load({
                url:this.remote_url,
                params: {start:0, limit: this.page_size}
        });
    }
}

CURRENT_PROTO.search_column = function(){return 'name'}
FW.Widgets.DataGrid.render_chop=function(data_index, colModel, str) {
    var sizing_name = 'sizing-element';
    var e = Ext.get(sizing_name);
    if(!e) {
        Ext.DomHelper.append(Ext.getBody(), {
            id:sizing_name,
            tag:'span',
            style: 'visibility:hidden'
        });
        e = Ext.get(sizing_name);
    }
    e.update(str);
    var str_width = e.getComputedWidth();
    var col_width = 0;
    var idx = colModel.config.grep(function(i) { return i.dataIndex == data_index; });
    if(idx >= 0)
        col_width = colModel.config[idx].width;
    if(col_width && col_width <= str_width) {
        var num_chars = str.length;
        var avg_width = str_width/num_chars;
        var chars_fit = Math.floor(col_width/avg_width);

        var ellipses = 3;
        var size = chars_fit - 3;
        var stripped = false;
        var comment = str;
        var c = comment;
        if(comment.length > size) {
            c=c.substring(0, size);
            stripped = true;
        }
        if(stripped) {
            var qwidth = Math.min(str_width, 500);
            var qtip = comment;
            if(str_width > qwidth) {
                chars_fit = qwidth/avg_width;
                size = chars_fit - 3;
                qtip = qtip.substring(0,size) + '...';
            }
            c = String.format("<span ext:qtip='{0}' ext:qwidth='{1}'>{2}...</span>", String.escape(qtip), qwidth, c);
        }
        return c; 
    }
    return str;
}


//http://extjs.com/forum/showthread.php?t=17219
Ext.grid.GridView.override({
    scrollToBottom : function() {
    	var s = this.scroller.dom;
    	s.scrollTop = s.scrollHeight;
    	s.scrollLeft = 0;
    }
});

///////// Editor Data Grid
Ext.ns('FW.Widgets.EditorDataGrid');
FW.Widgets.EditorDataGrid=function(config) {
    config.__editor_grid=true;
    FW.Widgets.DataGrid.call(this,config);
    config.clicksToEdit = 1;
    config.listeners = config.listeners || {};
    this.create_listener_sequence('beforeedit', config.listeners, function(obj) {
                                    this.edit_field_obj = obj;
                            }

    );
    FW.Widgets.EditorDataGrid.superclass.constructor.call(this,config);
}
Ext.extend(FW.Widgets.EditorDataGrid, Ext.grid.EditorGridPanel);
CURRENT_PROTO=FW.Widgets.EditorDataGrid.prototype;
for(var k in FW.Widgets.DataGrid.prototype) {
    if(k.match(/^_create/))
        CURRENT_PROTO[k] = FW.Widgets.DataGrid.prototype[k];
}
Ext.reg('fw.widgets.editordatagrid', FW.Widgets.EditorDataGrid);

CURRENT_PROTO.initComponent=function() {
    FW.Widgets.DataGrid.prototype.initComponent.call(this);
    var content = {};
    FW.Widgets.EditorDataGrid.superclass.initComponent.call( Ext.apply(this,content) );
}
