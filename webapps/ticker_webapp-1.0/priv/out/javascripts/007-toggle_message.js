FW.utils.ToggleMessage = function() {
    this.template = new Ext.XTemplate(
        '<div class="msg" style="margin-top:15px;">',
            '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
        	'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc">{s}</div></div></div>',
            '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
        '</div>'
    );
    this.template.compile();
    this.msgCt = Ext.DomHelper.insertFirst(document.body, {id:'toggle-msg-div'}, true);
}
CURRENT_PROTO = FW.utils.ToggleMessage.prototype;

CURRENT_PROTO.toggle_msg = function(title, message) {
    var m=this._create_msg_box(title,message);
    m.slideIn('t').pause(1).ghost("t", {remove:true});
}

CURRENT_PROTO.show_msg=function(title, message) {
    if(this.msg_box) {
        this.msg_box.remove();
        this.msg_box = null;
    }
    if(title && !message)
        message = title;
    this.msg_box=this._create_msg_box(title,message);
    this.msg_box.fadeIn('t', {duration:0.1});
}

CURRENT_PROTO.hide_msg=function() {
    if(this.msg_box) {
        this.msg_box.fadeOut({remove:true});
        this.msg_box = null;
    }
}

CURRENT_PROTO._create_msg_box=function(title,message) {
    this.msgCt.alignTo(document, 't-t');
    this.msgCt.alignTo(document, 't-t'); // do it twice..IE7 seems to need it!
    var box = this.template.applyTemplate({t:title, s:message});
    return Ext.DomHelper.append(this.msgCt, {html:box}, true);
}
