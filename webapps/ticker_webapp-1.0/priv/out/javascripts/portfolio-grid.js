Ext.ns('Yft.GFinance.PortfolioGrid');
Yft.GFinance.PortfolioGrid=function(config) {
    FW.Widgets.IBaseObj.fw_init.call(this,config);
    this.required_args(config, [], 'Yft.GFinance.PortfolioGrid');
    this.INTERVAL = 60*1000;
    Yft.GFinance.PortfolioGrid.superclass.constructor.call(this,config);
}
Ext.extend(Yft.GFinance.PortfolioGrid, FW.Widgets.DataGrid, FW.Widgets.IBaseObj);
CURRENT_PROTO=Yft.GFinance.PortfolioGrid.prototype;
Ext.reg('yft.gfinance.portfoliogrid',Yft.GFinance.PortfolioGrid);

CURRENT_PROTO.initComponent=function() {
    this.addEvents('add-new-portfolio','edit-clicked');
    this.actions = new Ext.XTemplate(
        '<span class="link" id="edit-{id}">edit</span>&nbsp;<span class="link" id="delete-{id}">delete</span>',
        '&nbsp;<span class="link" id="trans-{id}">trans</span>'
    );
    this.actions.compile();
    var content = {
        title:'Portfolio List',
        col_model: [
            {
                header: 'Portfolio Name',
                dataIndex:'name',
                id: 'portfolio-name'                
            },
            {
                header: 'Created On',
                type:'datetime',
                width: 50
            },
            {
                header: 'Performance %',
                dataIndex: 'performance',
                renderer: function(c) {
                    var is_neg = false;
                    var ret =  String(c).number_with_precision(2);
                    if(Number(c) < 0) {
                        ret = String.format("<span class='negative-number'>({0})</span>", ret);
                    }
                    return ret;
                },
                width: 40
            },
            {
                header:'Action',
                width: 50,
                sortable: false,
                dataIndex:'id',
                renderer: function(c,m,r,ri) {
                    return this.actions.applyTemplate({
                        id: c
                    });
                }
            }
        ],
        tbar : [
            '->',
            {
                text: 'Add New Portfolio',
                handler: this._on_add_new,
                scope: this
            }
        ],
        reader_root: 'page_data.portfolio_list',
        reader_total_property:'page_data.portfolio_list_count',
        remote_url: this.url('/portfolios'),
        autoExpandColumn:'portfolio-name',
        page_size: 10,
        frame: true,
        add_row_numbers: true,
        viewConfig: {
            scrollOffset: 0
        }
    };
    Yft.GFinance.PortfolioGrid.superclass.initComponent.call(Ext.apply(this,content));
}

CURRENT_PROTO.afterRender=function() {
    Yft.GFinance.PortfolioGrid.superclass.afterRender.call(this);
    this.on('rowclick', this._on_row_click, this);
    this.getStore().setDefaultSort('name','asc');
    this.getStore().load({params: {start: 0, limit: this.page_size, sort:'name', dir: 'asc'}});
    this.timer = new Ext.util.DelayedTask();
    this.timer.delay(this.INTERVAL, this.repeat, this);
}

CURRENT_PROTO.repeat=function() {
    this.getStore().reload();
    this.timer.delay(this.INTERVAL, this.repeat, this);
}

CURRENT_PROTO._on_row_click=function(g,ri,e) {
    var r=g.getStore().getAt(ri);
    if(e.within_el('edit-'+r.id)) {
        //this.fireEvent('edit-clicked', r.id);
        this.xhr({
            url: this.url('/portfolios', {params: {id: r.id}})
        });
    } else if(e.within_el('delete-'+r.id)) {
        Ext.Msg.confirm('Delete','Delete ' + r.get('name') + '?', function(ans) {
            if(ans == 'yes') {
                this.toggle_msg_obj().show_msg('Working...');
                this.xhr({
                    url: this.url('/portfolios'),
                    params: {id : r.id},
                    method: 'delete',
                    success: function(r) {
                        r=this.ajax_process_notice(r);
                        this.toggle_msg_obj().hide_msg();
                        this.getStore().reload();
                    }
                });
            }
        }, this);
    }
}

CURRENT_PROTO._on_add_new=function() {
    this.fireEvent('add-new-portfolio');
}

