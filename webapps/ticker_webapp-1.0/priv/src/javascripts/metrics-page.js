Ext.ns('Yft.GFinance.MetricsPage');
Yft.GFinance.MetricsPage = function (config) {
    Yft.GFinance.MetricsPage.superclass.constructor.call(this, config);
}
Ext.extend(Yft.GFinance.MetricsPage, FW.Pages.BaseObj);
CURRENT_PROTO = Yft.GFinance.MetricsPage.prototype;

CURRENT_PROTO.init = function() {
    this.tabs = new Ext.TabPanel({
        renderTo: 'metrics-div',
        activeTab: 0,
        width: 600,
        height: 500,
        items: [
            {
                title:'Volume',
                html: 'volume-metrics-panel'
            },
            {
                title:'Price',
                html: 'Price Movement'
            }
        ]
    });
}

