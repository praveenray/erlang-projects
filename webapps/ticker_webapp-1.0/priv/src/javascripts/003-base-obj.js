// base base
FW.BaseObj = function(config) {
}
CURRENT_PROTO = FW.BaseObj.prototype;
CURRENT_PROTO.data_store=function(ds) {
    if(ds) this._data_store = ds;
    return this._data_store;
}
CURRENT_PROTO.context=function(ctx) {
    if(ctx) {
        FW.BaseObj.prototype.site_context = ctx;
        Ext.BLANK_IMAGE_URL = FW.BaseObj.prototype.site_context + '/javascripts/001-ext-js/resources/images/default/s.gif';
    }
    return FW.BaseObj.prototype.site_context;
}

CURRENT_PROTO.authenticity_token=function(token) {
    if(token)
        FW.BaseObj.prototype.form_authenticity_token=token;
    return FW.BaseObj.prototype.form_authenticity_token;
}

CURRENT_PROTO.lsnr=function() {
    var listener = {};
    for(var i=0; i < arguments.length; i+=2) {
        listener[arguments[i]] = {
            fn: arguments[i+1],
            scope: this
        };
    }
    return listener;
}

Ext.override(FW.BaseObj, FW.utils.AjaxUtils);
Ext.override(FW.BaseObj, FW.utils.FormUtils);
Ext.override(FW.BaseObj, FW.utils.GridUtils);

CURRENT_PROTO.h = function(str) {
    return Ext.util.Format.htmlEncode(str);
}

CURRENT_PROTO.xhr = function(opts) {
    if(!opts.scope)
        opts.scope = this;
    if(opts.method)
        opts.method = opts.method.toUpperCase();
    if(opts.method == 'PUT' || opts.method == 'DELETE') {
        var params = opts.params || {};
        params['_method'] = opts.method;
        opts.method = 'POST';
        opts.params = params;
    }
    if(opts.method == 'POST') {
        var params = opts.params || {};
        if(!params.authenticity_token)
            params.authenticity_token = page.authenticity_token();
        opts.params = params;
    }
    if(opts.response) {
        opts.success = opts.failure = opts.response;
        delete opts.response;
    }
    if(!opts.failure) {
        opts.failure = function(r) {
            this.msg_obj().show_error("XHR Server Error");
        }
    }
    return Ext.Ajax.request(opts);
}

CURRENT_PROTO.bind=function(fn_obj) {
    return fn_obj.createDelegate(this);
}

CURRENT_PROTO.msg_obj=function() {
    var tmsg = page._o_transparent_message;
    if(!tmsg) {
        tmsg = page._o_transparent_message = FW.Widgets.TransparentMessage;
    }
    return tmsg;
}

CURRENT_PROTO.toggle_msg_obj=function() {
    var tmsg = page._o_toggle_message;
    if(!tmsg) {
        tmsg = page._o_toggle_message = new FW.utils.ToggleMessage();
    }
    return tmsg;
}

CURRENT_PROTO.url = function(orig_url, options) {
    options = options || {};
    var re = new RegExp('^'+this.context());
    if(!orig_url.match(re)) {
        orig_url = orig_url.replace(/^\//,'');
        orig_url =  this.context() + '/'+ orig_url;
    }
    if(options.params)
        orig_url=orig_url+'?'+Ext.urlEncode(options.params);

    return orig_url;
}

CURRENT_PROTO.refresh_page = function(new_url, options) {
    var u   = new_url;
    options = options || {};

    if(options.params)
        u=u+'?'+Ext.urlEncode(options.params);

    if(!u.match(/^http/))
        u = this.url(u);

    if(options.msg)
        this.toggle_msg_obj().show_msg('', options.msg);
    window.location = u;
}

CURRENT_PROTO.required_args=function(config, arg_list, class_name) {
    arg_list = Ext.isArray(arg_list) ? arg_list : [arg_list];
    Ext.each(arg_list, function(i) {
        if(Ext.isEmpty(config[i]))
            throw new Error("Missing Required argument ("+i+") from "+class_name);
    },this);
}

CURRENT_PROTO.create_cookie = function(name,value,days) {
    if (days) {
	var date = new Date();
	date.setTime(date.getTime()+(days*24*60*60*1000));
	var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

CURRENT_PROTO.read_cookie = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
	var c = ca[i];
	while (c.charAt(0)==' ') c = c.substring(1,c.length);
	if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

CURRENT_PROTO.erase_cookie = function(name) {
    this.create_cookie(name,"",-1);
}

// Pages
FW.Pages.BaseObj = function(config) {
    config               = config || {};
    this._rbac = {};
    this.on_ready_events = [];
    FW.Pages.BaseObj.superclass.constructor.call(this,config);
    this._page_json_data = config.page_json_data;
    this.camelize_cache = {};
}
Ext.extend(FW.Pages.BaseObj, FW.BaseObj);
CURRENT_PROTO = FW.Pages.BaseObj.prototype;

CURRENT_PROTO.on_ready=function(fn_obj) {
    this.on_ready_events.push(fn_obj);
}
CURRENT_PROTO.fire_on_ready_events=function() {
    Ext.each(this.on_ready_events, function(fn) {
        fn.call(this);
    },this);
}

CURRENT_PROTO.data_model = function(data) {
    if(data != null) {
        this._page_json_data = data;
        this.data_store(new FW.DataStore(data.page_data));
    }
    return this._page_json_data;
}

CURRENT_PROTO.page_rbac=function(r) {
    if(r)
        this._rbac = r;
    return this._rbac;
}
///
FW.Widgets.BaseObj = function(config) {
    config  = config || {};
    FW.Pages.BaseObj.superclass.constructor.call(this,config);

    this.parent_container_id = config.parent_container_id || config.parent_id;
	if(config.data_store)
		this.data_store(config.data_store);
	else if(page && page.data_store)
		this.data_store(page.data_store());
    this._rbac  = config.rbac || {};
    this.__id_prefix = config.id_prefix || config.id || null;
};
Ext.extend(FW.Widgets.BaseObj, FW.BaseObj);
CURRENT_PROTO=FW.Widgets.BaseObj.prototype;

CURRENT_PROTO.rbac = function(r) {
    if(r)
        this._rbac = r;
    return this._rbac;
}

CURRENT_PROTO._render=function(parent_container_cmp, content) {
    if(parent_container_cmp) {
        parent_container_cmp.add(content);
    }
    return content;
}
CURRENT_PROTO.child_id=function(childid) {return this.id_prefix() + '-' + childid;  this.top_level_id() + childid }
CURRENT_PROTO.child_cmp=function(childid) { return Ext.getCmp(this.child_id(childid)) }
CURRENT_PROTO.id_prefix=function() {
    if(!this.__id_prefix)
        throw new Error("Parent Must Supply id or id_prefix");

    return this.__id_prefix;
 }
CURRENT_PROTO.own_id_prefix=function() {throw new Error('Removed. Must not call!')}
CURRENT_PROTO.top_level_id=function() {return this.child_id('main'); this.id_prefix() + this.own_id_prefix()}
CURRENT_PROTO.top_level_cmp=function() {
    var tid = this.top_level_id();
    if(!tid)
        throw new Error("top_level_id() is NULL");
    return Ext.getCmp(tid);
}
CURRENT_PROTO.after_create=function() {}

FW.Widgets.IBaseObj={};
for(var k in FW.Widgets.BaseObj.prototype) {
    if(k != 'override' && k != 'constructor') {
        FW.Widgets.IBaseObj[k] = FW.Widgets.BaseObj.prototype[k];
    }
}
FW.Widgets.IBaseObj.fw_init=function(config) {
    FW.Widgets.BaseObj.call(this,config);
    // delete these two since we've methods with same name and Ext.extend overwrites those two methods if config is passed to the Ext baseclass!
    delete config.rbac;
    delete config.id_prefix;
}
