Ext.ns('Yft.GFinance.TickersPage');
Yft.GFinance.TickersPage = function (config) {
    Yft.GFinance.TickersPage.superclass.constructor.call(this, config);
}
Ext.extend(Yft.GFinance.TickersPage, FW.Pages.BaseObj);
CURRENT_PROTO = Yft.GFinance.TickersPage.prototype;

CURRENT_PROTO.init = function() {
    this.grid = new Yft.GFinance.TickersGrid({
        width: 600,
        height: 350,
        bodyStyle:'padding: 10px 10px 10px;',
        renderTo: 'tickers-grid-div',
        frame: true,
        listeners: this.lsnr('edit-clicked', this._on_add_new_ticker)
    });
    this.stop_daemon = new Ext.Button({
        renderTo: 'stop-daemon-div',
        text:'Stop Price Daemon',
        handler: this._stop_price_daemon,
        scope: this
    });
}

CURRENT_PROTO._on_add_new_ticker=function(ticker_id) {
    this._close_ticker_form();

    var form = new Yft.GFinance.TickerForm({
        id:'ticker-form',
        height: (Ext.isEmpty(ticker_id) ? 200 : 225),
        ticker_id: ticker_id,
        listeners: this.lsnr('closed', this._close_ticker_form, 'saved', function() {
            this._close_ticker_form();
            this.grid.getStore().reload();
        })
    });

    this.ticker_form = new Ext.Window({
        modal: true,
        width: 475,
        height: 185,
        layout:'fit',
        title: (ticker_id ? 'Edit' : 'New') + ' Ticker Information ',
        frame: true,
        items: form
    });
    this.ticker_form.show();    
}

CURRENT_PROTO._close_ticker_form=function() {
    if(this.ticker_form) {
        this.ticker_form.destroy();
        this.ticker_form = null;
    }
}

CURRENT_PROTO._stop_price_daemon=function() {
    this.toggle_msg_obj().show_msg('Stopping...');
    this.xhr({
        url: this.url('/quotes'),
        method:'delete',
        params:{id: 'fake'},
        success: function(r) {
            this.toggle_msg_obj().hide_msg();
            r=this.ajax_parse_response(r);
        }
    });
}
