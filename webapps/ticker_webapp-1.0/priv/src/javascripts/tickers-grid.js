// Grid
Ext.ns('Yft.GFinance.TickersGrid');
Yft.GFinance.TickersGrid=function(config) {
    FW.Widgets.IBaseObj.fw_init.call(this,config);
    this.required_args(config, [], 'Yft.GFinance.TickersGrid');
    Yft.GFinance.TickersGrid.superclass.constructor.call(this,config);
}
Ext.extend(Yft.GFinance.TickersGrid, FW.Widgets.DataGrid, FW.Widgets.IBaseObj);
CURRENT_PROTO=Yft.GFinance.TickersGrid.prototype;
Ext.reg('yft.gfinance.tickersgrid',Yft.GFinance.TickersGrid);

CURRENT_PROTO.initComponent=function() {
    this.addEvents('edit-clicked');
    this.actions = new Ext.XTemplate(
        "<span id='edit-{id}' class='link'>edit</span>&nbsp;",
        "<span id='delete-{id}' class='link'>delete</span>&nbsp;"
//        "<span id='chart-{id}' class='link'>chart</span>"
    );
    this.actions.compile();
    var content = {
        item_name:'Ticker',
        title:'Quotes List',
        col_model: [
            {
                header: 'Ticker',
                dataIndex: 'symbol',
                id: 'ticker' ,
                width:32
            },
            {
                header: 'Exchange',
                dataIndex:'exch',
                width: 35
            },
            {
                header: 'Active',
                width:28
            },
            {
                header: 'Actions',
                sortable:false,
                dataIndex:'id',
                renderer:function(c,m,r,ri) {
                    return this.actions.applyTemplate({id: c});
                }
            }
        ],
        tbar : [
            '->',
            'Tickers: ',
            {
                xtype:'textfield',
                id: 'tickers',
                width: 350
            },
            {
                text: 'Add',
                handler: this._on_add_tickers,
                scope: this
            }
        ],
        reader_root: 'page_data.tickers_list',
        reader_total_property:'page_data.total_count',
        remote_url: this.url('/tickers'),
        autoExpandColumn:'ticker',
        page_size: 10,
        frame: true,
        add_row_numbers: true,
        viewConfig: {
            scrollOffset: 0
        }
    };
    Yft.GFinance.TickersGrid.superclass.initComponent.call(Ext.apply(this,content));
}

CURRENT_PROTO.afterRender=function() {
    Yft.GFinance.TickersGrid.superclass.afterRender.call(this);
    new Ext.KeyNav(Ext.getCmp('tickers').getEl(), {
        enter: function(evt) {
            this._on_get_quotes();
        },
        scope: this
    });
    this.getStore().setDefaultSort('ticker','asc');
    this.getStore().load({params: {start: 0, limit: this.page_size, sort:'ticker', dir: 'asc'}});
    this.on('rowclick', this._on_row_click, this);
}

CURRENT_PROTO._on_row_click=function(g,ri,e) {
    var r = this.getStore().getAt(ri);
    if(e.within_el('edit-'+r.id)) {
        this.fireEvent('edit-clicked', r.id);
    } else if(e.within_el('delete-'+r.id)) {
        Ext.Msg.confirm('Delete','Delete ' + r.get('ticker') + ' ?', function(ans) {
            if(ans == 'yes') {
                this.getView().el.mask('Please Wait...');
                this.xhr({
                    url: this.url('/tickers'),
                    params: {id: r.id},
                    method:'delete',
                    success:function(r) {
                        this.getView().el.unmask();
                        this.getStore().reload();
                    },
                    scope: this
                });
            }
        }, this);
    }
}

CURRENT_PROTO._on_add_tickers=function() {
    var tickers = Ext.getCmp('tickers').getValue().trim();

    if(tickers.length > 0) {
        this.getView().el.mask('Please Wait...');
        this.xhr({
            url: this.url('/tickers'),
            method:'post',
            params: {tickers: tickers},
            success: function(r) {
                r=this.ajax_parse_response(r);
                this.getView().el.unmask();
                this.getStore().reload();
                if(!r.success) {
                    this.ajax_process_errors(r);
                } else {
                    Ext.getCmp('tickers').setValue('');
                }
            }
        });
    }
}


