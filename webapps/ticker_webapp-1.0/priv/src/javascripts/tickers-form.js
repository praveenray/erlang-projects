Ext.ns('Yft.GFinance.TickerForm');
Yft.GFinance.TickerForm=function(config) {
    FW.Widgets.IBaseObj.fw_init.call(this,config);

    if(config.ticker_id) {
        config.edit_data_root = 'ticker_data';
        config.edit_url = this.url('/tickers/', {params: {id: config.ticker_id}});
    }
    Yft.GFinance.TickerForm.superclass.constructor.call(this,config);
}

Ext.extend(Yft.GFinance.TickerForm, FW.Widgets.form.FormPanel, FW.Widgets.IBaseObj);
CURRENT_PROTO=Yft.GFinance.TickerForm.prototype;
Ext.reg('yft.gfinance.tickerform',Yft.GFinance.TickerForm);

CURRENT_PROTO.initComponent=function() {
    this.addEvents('closed','saved');
    var content = {
        frame: true,
        edit_url: this.edit_url,
        edit_data_root: 'ticker_data',
        name_prefix: 'ticker',
        defaults: {width: 325, xtype:'textfield'},
        items: [
            {
                name: 'ticker',
                id:'ticker',
                allowBlank: false,
                maxLength: 50
            },
            {
                name:'exch',
                fieldLabel:'Exchange',
                id:'exchange',
                maxLength: 50
            },
            {
                name:'active',
                xtype:'combo',
                triggerAction:'all',
                editable:false,
                width: 70,
                store: [['Y','Yes'],['N','No']]
            }
        ],
        buttons: [
            {
                text: 'Save',
                handler: this._on_save,
                scope: this
            },
            {
                text:'Close',
                handler: this._on_close,
                scope: this
            }
        ]
    };

    Yft.GFinance.TickerForm.superclass.initComponent.call(Ext.apply(this, content));
}

CURRENT_PROTO._on_save = function() {
    var params = (this.ticker_id ? {id: this.ticker_id} : {} );
    this.submit({
        url: this.ticker_id ? this.edit_url :  this.url('/tickers'),
        method: (this.ticker_id ? 'put' : 'post'),
        success: function(r) {
            r=this.ajax_process_notice(r);
            this.fireEvent('saved', r.page_data.ticker_id);
        },
        scope:this
    });
}

CURRENT_PROTO._on_close=function() {
    this.fireEvent('closed');
}
