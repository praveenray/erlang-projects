Ext.namespace('YFT.PoweredBy');
YFT.PoweredBy = function(config) {
    config = config || {};
    YFT.PoweredBy.superclass.constructor.call(this, config);
    this.container = config.container || 'power-by-div';
    this.init();
}
Ext.extend(YFT.PoweredBy, FW.Pages.BaseObj);
CURRENT_PROTO = YFT.PoweredBy.prototype;

CURRENT_PROTO.init = function() {
    if(!Ext.get(this.container))
        return;
    var store = new Ext.data.JsonStore({
        root: 'records',
        totalProperty: 'total_count',
        idProperty: 'id',
        remoteSort: true,

        fields: ['image_string'],

        proxy: new Ext.data.ScriptTagProxy({
            url: 'http://www.yellowfish.biz/powered_by_coda'
        }),
        listeners: this.lsnr('load', function(s) {
            var r=store.getAt(0);
            Ext.get(this.container).update(r.get('image_string'));
        })
    });
    store.load();
}
