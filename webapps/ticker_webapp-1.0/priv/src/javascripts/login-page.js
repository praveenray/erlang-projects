Ext.namespace('Yft.GFinance.LoginPage');
Yft.GFinance.LoginPage = function(config) {
    Yft.GFinance.LoginPage.superclass.constructor.call(this, config);
}
Ext.extend(Yft.GFinance.LoginPage, FW.Pages.BaseObj);
CURRENT_PROTO = Yft.GFinance.LoginPage.prototype;

CURRENT_PROTO.init = function(config) {
    this.login_form = new FW.Widgets.form.FormPanel({
        frame: true,
        width: 350,
        height: 200,
        defaults: {xtype: 'textfield', width: 150, allowBlank: false},
        labelWidth: 80,
        name_prefix:'google_info',
        items: [
            {
                name: 'google_user_name'                
            },
            {
                name:'google_password'
            }
        ],
        buttons: [
            {
                text: 'Login',
                handler: this._on_login,
                scope: this
            }
        ],
        renderTo: 'login-form'
    });
    
}

CURRENT_PROTO._on_login=function() {
    if(this.login_form.getForm().isValid()) {
        this.login_form.submit({
            url: this.url('/sessions'),
            method:'POST',
            success: function(r) {
                if(r.success) {
                    this.ajax_process_notice(r);
                }
            },
            scope: this
        });
    }    
}
