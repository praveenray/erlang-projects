Ext.ns('Yft.GFinance.PriceVolSwingGrid');
Yft.GFinance.PriceVolSwingGrid=function(config) {
    FW.Widgets.IBaseObj.fw_init.call(this,config);
    this.required_args(config, [], 'Yft.GFinance.PriceVolSwingGrid');
    Yft.GFinance.PriceVolSwingGrid.superclass.constructor.call(this,config);
}
Ext.extend(Yft.GFinance.PriceVolSwingGrid, FW.Widgets.DataGrid, FW.Widgets.IBaseObj);
CURRENT_PROTO=Yft.GFinance.PriceVolSwingGrid.prototype;
Ext.reg('yft.gfinance.portfoliogrid',Yft.GFinance.PriceVolSwingGrid);

CURRENT_PROTO.initComponent=function() {
    var content = {
        title:'Price Volume Upswings',
        col_model: [
            {
                header: 'Symbol',                                
                id: 'symbol',
            },
            {
                header: 'Last Price',
                sortable:false,
                width: 50
            },
            {
                header: 'Last Volume',                
                width: 70
            }
        ],
        reader_root: 'page_data.price_swing_list',
        reader_total_property:'page_data.total_count',
        remote_url: this.url('/price_volume_upswings'),
        autoExpandColumn:'symbol',
        page_size: 10,
        frame: true,
        add_row_numbers: true,
        viewConfig: {
            scrollOffset: 0
        }
    };
    Yft.GFinance.PriceVolSwingGrid.superclass.initComponent.call(Ext.apply(this,content));
}

CURRENT_PROTO.afterRender=function() {
    Yft.GFinance.PriceVolSwingGrid.superclass.afterRender.call(this);
    this.on('rowclick', this._on_row_click, this);
    this.getStore().setDefaultSort('volume','desc');
    this.getStore().load({params: {start: 0, limit: this.page_size, sort:'volume', dir: 'desc'}});
}


