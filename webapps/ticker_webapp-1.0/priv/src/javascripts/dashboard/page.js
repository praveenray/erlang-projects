Ext.ns('Yft.GFinance.DashboardPage');
Yft.GFinance.DashboardPage = function (config) {
    Yft.GFinance.DashboardPage.superclass.constructor.call(this, config);
}
Ext.extend(Yft.GFinance.DashboardPage, FW.Pages.BaseObj);
CURRENT_PROTO = Yft.GFinance.DashboardPage.prototype;

CURRENT_PROTO.init = function() {
    this.price_vol_swing_grid = new Yft.GFinance.PriceVolSwingGrid({
        width: 300,
        height: 300,
        renderTo: 'price-vol-swing-div'
    });
}
