%%%-------------------------------------------------------------------
%%% File    : ticker_webapp_app.erl
%%% Author  : Praveen Ray <praveen@lenovo>
%%% Description : 
%%%
%%% Created : 21 Jun 2009 by Praveen Ray <praveen@lenovo>
%%%-------------------------------------------------------------------
-module(ticker_webapp).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1,start_phase/3]).

%%====================================================================
%% Application callbacks
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start(Type, StartArgs) -> {ok, Pid} |
%%                                     {ok, Pid, State} |
%%                                     {error, Reason}
%% Description: This function is called whenever an application 
%% is started using application:start/1,2, and should start the processes
%% of the application. If the application is structured according to the
%% OTP design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%%--------------------------------------------------------------------
start(_Type, _StartArgs) ->
    Args = lists:map(
       fun(Var) -> {ok, Value} = application:get_env(?MODULE, Var), Value end,
       [port, working_dir, yaws_dir]
    ),
    ticker_webapp_sup:start_link(Args)
.

%%--------------------------------------------------------------------
%% Function: stop(State) -> void()
%% Description: This function is called whenever an application
%% has stopped. It is intended to be the opposite of Module:start/2 and
%% should do any necessary cleaning up. The return value is ignored. 
%%--------------------------------------------------------------------
stop(_State) ->
    ok.

start_phase(log4erl,_,Conf) ->
    {conf_file, FileName} = Conf,
    {ok, WorkingDir} = application:get_env(?MODULE, working_dir),
    FullConfPath = filename:join(WorkingDir, FileName),
    application:load(log4erl),
    application:start(log4erl),
    log4erl:conf(FullConfPath),
    ok
    ;

start_phase(sasl, _, _) ->
    application:start(sasl);

start_phase(mnesia, _, _) ->
    % {ok, MnesiaDir} = application:get_env(?MODULE, mnesia_dir),
    % application:load(mnesia),
    % application:set_env(mnesia, dir, MnesiaDir),
    application:start(mnesia),
    mnesia:wait_for_tables([price], infinity)    
.

%%====================================================================
%% Internal functions
%%====================================================================
