-module(tw_controller_price_volume_upswings).
-include("../../../yaws/lib/yaws/include/yaws.hrl").
-include("../../../yaws/lib/yaws/include/yaws_api.hrl").
-include_lib("stdlib/include/qlc.hrl").
-include("../../../ticker_database-1.0/include/td_includes.hrl").

-export([index/2]).

index({arg,Arg},{errors, ErrorObj}) ->
    SortDir = yft_model_base:extract_sort_dir(Arg),
    Sort_col= yft_model_base:extract_sort_col(Arg, "volume"),
    Results = yft_model_base:display_find(Arg, price_volume_upswing, 
                                          fun(A,B) ->
                                                  case Sort_col of
                                                      "last_volume" -> 
                                                          yft_model_base:order_a_column(SortDir, A#price_volume_upswing.last_volume, B#price_volume_upswing.last_volume);
                                                      "symbol" -> 
                                                          yft_model_base:order_a_column(SortDir, A#price_volume_upswing.symbol, B#price_volume_upswing.symbol);
                                                      "last_price" ->
                                                          yft_model_base:order_a_column(SortDir, A#price_volume_upswing.last_price, B#price_volume_upswing.last_price)
                                                  end
                                          end,
                                          fun build_struct/1,
                                          "price_swing_list"
     ),
    
    [{ok, Results}, {errors, ErrorObj}]
.

build_struct(Price) ->
    {struct,
     [
      {<<"symbol">>, list_to_binary(Price#price_volume_upswing.symbol)},
      {<<"date">>, Price#price_volume_upswing.date},
      {<<"last_price">>,  Price#price_volume_upswing.last_price},
      {<<"last_volume">>, Price#price_volume_upswing.last_volume}
     ]
    }
.

