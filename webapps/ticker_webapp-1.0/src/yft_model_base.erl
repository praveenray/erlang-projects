-module(yft_model_base).
-include("../../../yaws/lib/yaws/include/yaws_api.hrl").
-include_lib("stdlib/include/qlc.hrl").
-export([display_find/4, display_find/5, extract_start_limit/1, extract_sort_dir/1, extract_sort_col/2, order_a_column/3]).


display_find(Arg, Table, Order, BuildStructFun) ->
    display_find(Arg, Table, Order, BuildStructFun, "records").

display_find(Arg, Table, Order, BuildStructFun, RecordsKey) ->
    [Start,Limit] = extract_start_limit(Arg),
    F = fun() ->
                Query     = qlc:q([M || M <- mnesia:table(Table)]),
                SortQuery = qlc:sort(Query, [{order, Order }]),
                Cursor    = iterate_cursor(qlc:cursor(SortQuery), Start, 0),
                Results   = [BuildStructFun(M) || M <- qlc:next_answers(Cursor, Limit)],
                qlc:delete_cursor(Cursor),
                Results
        end,
    {atomic, Results} = mnesia:transaction(F),
    {struct,[{list_to_binary(RecordsKey), Results},
             {<<"total_count">>, mnesia:table_info(Table,size)}
            ]
    }
.    

extract_sort_dir(Arg)  when is_record(Arg,arg) ->
    Dir = case yaws_api:getvar(Arg,"dir") of 
              undefined  -> "asc";
              {ok,Other} -> Other
          end,

    case string:to_lower(Dir) of
        "asc"  -> asc;
        "desc" -> desc
    end
.   

extract_sort_col(Arg, Default_col) when is_record(Arg,arg) ->
    case yaws_api:getvar(Arg, "sort") of
        undefined  -> Default_col;
        {ok,Other} -> string:to_lower(Other)
    end
.

order_a_column(Sort_dir, A, B) ->
    case Sort_dir of
        asc  -> A < B;
        desc -> A > B
    end
.    

extract_start_limit(Arg) when is_record(Arg,arg) ->
    Start = case (yaws_api:getvar(Arg,"start")) of
                undefined  -> "0";
                {ok,Other} -> Other
            end,
                        
    Limit = case (yaws_api:getvar(Arg,"limit")) of
                undefined -> "10";
                {ok,Int}  -> Int
            end,
    [StartInteger, LimitInteger] = [X || {X,_} <- [string:to_integer(Start), string:to_integer(Limit)], X /= error ],
    log4erl:debug("Start = ~p, Limit = ~p~n", [StartInteger, LimitInteger]),
    [StartInteger,LimitInteger]
.

iterate_cursor(Cursor, Start, Accum) when Start == Accum -> Cursor;
iterate_cursor(Cursor, Start, Accum) ->
    case (qlc:next_answers(Cursor, 1)) of
        [] -> Cursor;
        _  -> iterate_cursor(Cursor,Start, Accum+1)
    end
.
            
