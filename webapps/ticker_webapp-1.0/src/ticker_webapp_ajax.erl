-module(ticker_webapp_ajax).
-export([out/1]).
-include("../../../yaws/lib/yaws/include/yaws.hrl").
-include("../../../yaws/lib/yaws/include/yaws_api.hrl").


out(Arg) ->
    yft_controller_base:dispatch(Arg,ticker_webapp)
.

%%%%%%%%%%%%%%%%%%%% Not Used %%%%%%%%%%%%%%%%
%% outA(Arg) ->
%%     Resp = mochijson2:encode({struct,[{<<"records">>,
%%                      [
%%                       {struct, [
%%                                 {<<"name">>, list_to_binary("Praveen")},
%%                                 {<<"age">>, 30}
%%                                ]
%%                       },
%%                       {struct, [
%%                                 {<<"name">>, list_to_binary("Daisy")},
%%                                 {<<"age">>, 23}
%%                                ]
%%                       }
%%                      ]
%%                     }
%%                    ]      
%%     }),
%%     {ehtml, {xhtml, [], 
%%              [{head,[],
%%                [{title, [], "Ajax Test Landing Page"}]},
%%               {body, [{style, "background-color:green;color:white;"}],
%%                [{table,[],
%%                  [{tr,[],
%%                    [{td, [], "Path Before:"},
%%                     {td, [], Arg#arg.appmod_prepath}]},
%%                   {tr,[],
%%                    [{td,[], "Path After:"},
%%                     {td,[], Arg#arg.appmoddata, Arg }]}
%%                  ]}
%%                ]}
%%               ]}
%%     },
%%     % {html, io_lib:format("HELLO WORLD FROM ticker_webapp_ajax <pre>~p</pre>", [list_to_binary(Resp)])}
%%     % {html, io_lib:format("<pre>~p</pre><br/>", [A])}

%%     [M,F,A] = tw_controller_base:rest_resolver(Arg, "tw_controller_"),
%%     yft_controller_base:process_output(erlang:apply(M,F,A), Arg)
%% .
