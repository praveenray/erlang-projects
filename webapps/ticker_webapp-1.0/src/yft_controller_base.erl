-module(yft_controller_base).
-include("../../../yaws/lib/yaws/include/yaws.hrl").
-include("../../../yaws/lib/yaws/include/yaws_api.hrl").
-export([rest_resolver/2, process_output/2, dispatch/2]).


dispatch(Arg, ProjectModule) when is_record(Arg,arg) ->
    {ok,Prefix} = application:get_env(ProjectModule, project_prefix),
    [M,F,A]     = rest_resolver(Arg, Prefix ++ "controller_"),
    process_output(erlang:apply(M,F,A), Arg)
.    

process_output(Output, _Arg) ->
    [{ok, Results},{errors, ErrorObj}] = Output,
    
    Success = not(yft_controller_error_obj:has_errors(ErrorObj)),
    Resp    = {struct, [{<<"success">>, Success},
                        {<<"page_data">>, Results},
                        {<<"error_obj">>, {struct, yft_controller_error_obj:build_struct(ErrorObj)}}
              ]},
    {content, "application/json",list_to_binary(mochijson2:encode(Resp))}
.


rest_resolver(Arg, ModulePrefix) ->
    Suffix = get_suffix(Arg),

    ModuleName = ModulePrefix ++ 
        lists:last(string:tokens(re:replace(re:replace(Suffix, "^/", "",[{return,list}]), "/$", "",[{return,list}]) ,"/")),

    log4erl:debug("ModuleName Resolved to: ~s", [ModuleName]),
    
    MethodName = case [(Arg#arg.req)#http_request.method, yaws_api:getvar(Arg,"_method")] of
                    ['POST', "PUT"]    -> 'PUT';
                    ['POST', "DELETE"] -> 'DELETE';
                    [M,_]              -> M
                 end,
    
    log4erl:debug("Method: ~s", [MethodName]),
    Id = case yaws_api:getvar(Arg,"id") of
           {ok,Value} -> string:strip(Value);
           _          -> undefined
    end,

    Method = controller_method(MethodName, Id),
    IdList = case Id of
                undefined -> [];
                _     -> [{id, Id}]
             end,

    [list_to_atom(ModuleName), Method, lists:append([{arg,Arg},{errors, yft_controller_error_obj:new()}], IdList)]
.

controller_method('GET', undefined) ->
    index;
controller_method('GET', _Id) ->
    show;
controller_method('POST', undefined) ->
    new;
controller_method('POST', _Id) ->
    create;
controller_method('PUT', Id) when Id /= undefined -> 
    update;
controller_method('DELETE', Id) when Id /= undefined ->
    delete.
    
get_suffix(Arg) ->
    case (Arg#arg.appmoddata) of
        undefined -> "/";
        null      -> "/";
        Other     -> Other
    end.
