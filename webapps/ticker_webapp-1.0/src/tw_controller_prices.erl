-module(tw_controller_prices).
-include("../../../yaws/lib/yaws/include/yaws.hrl").
-include("../../../yaws/lib/yaws/include/yaws_api.hrl").
-include_lib("stdlib/include/qlc.hrl").
-include("../../../ticker_database-1.0/include/td_includes.hrl").

-export([index/2]).

index({arg,Arg},{errors, ErrorObj}) ->
    SortDir = yft_model_base:extract_sort_dir(Arg),
    Results = yft_model_base:display_find(Arg, price, 
                                          fun(A,B) -> 
                                              case SortDir of
                                                  asc  -> A#price.symbol < B#price.symbol;
                                                  desc -> A#price.symbol > B#price.symbol
                                              end
                                          end,
                                          fun build_struct/1,
                                          "price_list"
     ),
    
    [{ok, Results}, {errors, ErrorObj}]
.

build_struct(Price) ->
    {struct,
     [
      {<<"symbol">>, list_to_binary(Price#price.symbol)},
      {<<"close">>, list_to_binary(Price#price.close)},
      {<<"open">>, list_to_binary(Price#price.open)},
      {<<"volume">>, list_to_binary(Price#price.volume)},
      {<<"date">>, list_to_binary(Price#price.date)}
     ]
    }
.

