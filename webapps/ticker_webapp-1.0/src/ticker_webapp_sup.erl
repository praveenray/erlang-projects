%%%-------------------------------------------------------------------
%%% File    : ticker_webapp_sup.erl
%%% Author  : Praveen Ray <praveen@lenovo>
%%% Description : 
%%%
%%% Created : 21 Jun 2009 by Praveen Ray <praveen@lenovo>
%%%-------------------------------------------------------------------
-module(ticker_webapp_sup).

-behaviour(supervisor).

%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start_link() -> {ok,Pid} | ignore | {error,Error}
%% Description: Starts the supervisor
%%--------------------------------------------------------------------
start_link(Args) ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, [Args]).

%%====================================================================
%% Supervisor callbacks
%%====================================================================
%%--------------------------------------------------------------------
%% Func: init(Args) -> {ok,  {SupFlags,  [ChildSpec]}} |
%%                     ignore                          |
%%                     {error, Reason}
%% Description: Whenever a supervisor is started using 
%% supervisor:start_link/[2,3], this function is called by the new process 
%% to find out about restart strategy, maximum restart frequency and child 
%% specifications.
%%--------------------------------------------------------------------
init([Args]) ->

    Server = {'TickerWebAppServer',{'ticker_webapp_server',start_link,[Args]},
              permanent,2000,worker,['ticker_webapp_server']},
    {ok,{{one_for_all,2,10}, [Server]}}.

%%====================================================================
%% Internal functions
%%====================================================================
