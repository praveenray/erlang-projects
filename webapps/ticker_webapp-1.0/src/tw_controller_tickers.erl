-module(tw_controller_tickers).
-include("../../../yaws/lib/yaws/include/yaws.hrl").
-include("../../../yaws/lib/yaws/include/yaws_api.hrl").
-include_lib("stdlib/include/qlc.hrl").
-include("../../../ticker_database-1.0/include/td_includes.hrl").

-export([index/2]).

index({arg,Arg},{errors, ErrorObj}) ->
    SortDir = yft_model_base:extract_sort_dir(Arg),
    Results = yft_model_base:display_find(Arg, ticker, 
                                          fun(A,B) -> 
                                              case SortDir of
                                                  asc  -> A#ticker.symbol < B#ticker.symbol;
                                                  desc -> A#ticker.symbol > B#ticker.symbol
                                              end
                                          end,
                                          fun build_struct/1,
                                          "tickers_list"
     ),
    
    [{ok, Results}, {errors, ErrorObj}]
.

%% update({arg,Arg},{errors, ErrorObj}, {id, Id}) ->
%%     null
%% .

build_struct(Ticker) ->
    {struct,
     [
      {<<"symbol">>, list_to_binary(Ticker#ticker.symbol)},
      {<<"name">>, list_to_binary(Ticker#ticker.name)}      
     ]
    }
.
