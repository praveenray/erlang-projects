-module(yft_controller_error_obj).
-export([new/0, has_errors/1, has_notice/1, add_error_to_base/2, add_error_to_field/3, build_struct/1]).

new() ->
    D0 = dict:new(),
    D1 = dict:store(page, null, D0),
    D2 = dict:store(fields, dict:new(), D1),
    D3 = dict:store(notice, null, D2),
    D3
.

has_errors(ErrorDict) ->
    case [dict:find(page,ErrorDict), dict:find(fields, ErrorDict)] of
        [{ok,_}, _]      -> true;
        [error, Fields]  -> dict:size(Fields) > 0;
        [_,_] -> false
    end
.

has_notice(ErrorDict) ->
    case dict:find(notice, ErrorDict) of
        undefined -> false;
        _ -> true
    end
.

add_error_to_base(ErrorDict, Msg) ->
    dict:store(page,Msg, ErrorDict)
.

add_error_to_field(ErrorDict, Field, Msg) ->
    {ok, Fields} = dict:find(fields, ErrorDict),
    NewFields    = dict:store(Field, Msg, Fields),
    dict:store(fields, NewFields, ErrorDict)
.
            
build_struct(ErrorDict) ->    
    [
     {<<"page">>, list_to_binary(page_error(ErrorDict))},
     {<<"notice">>, list_to_binary(notice(ErrorDict)) }
    ]
.

page_error(ErrorDict) ->
    case dict:find(page,ErrorDict) of
        error -> "";
        {ok, Value} when (Value == null) or (Value == undefined) -> "";
        {ok, Other} -> Other
    end
.


notice(ErrorDict) ->
    case dict:find(notice, ErrorDict) of
        error -> "";
        {ok, Value} when (Value == null) or (Value == undefined) -> "";
        {ok, Other} -> Other
    end
.
