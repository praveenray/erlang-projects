-module(ta_price_volume_upswings).
-include_lib("stdlib/include/qlc.hrl").
-include("../../ticker_database-1.0/include/td_includes.hrl").

-export([run/3]).

run({StartY, StartM, StartD}, Ticker_list, Window_of_analysis) ->
    ok = run_now({StartY, StartM, StartD}, Ticker_list, Window_of_analysis)
.

run_now(_,[], _) -> ok;
run_now({StartY, StartM, StartD}, [H|L], Window_of_analysis) ->
    case (catch is_ok({StartY, StartM, StartD}, H, Window_of_analysis)) of
        {true, Last_price, Last_volume} ->
            mnesia:dirty_write(#price_volume_upswing{
                 symbol      = H,
                 last_price  = Last_price,
                 last_volume = Last_volume,
                 date        = calendar:date_to_gregorian_days(StartY,StartM,StartD)
            }),
            run_now({StartY, StartM, StartD}, L, Window_of_analysis);
        {false,_,_} -> 
            run_now({StartY, StartM, StartD}, L, Window_of_analysis);
        {'EXIT', Reason} -> 
            log4erl:warn("Price/Volume upswing analysis failed for Ticker ~s - ~p", [H,Reason]),
            run_now({StartY, StartM, StartD}, L, Window_of_analysis)
    end
.

is_ok({Y,M,D}, Ticker,Window_of_analysis) ->
    StartingDate     = calendar:date_to_gregorian_days(Y,M,D),
    O    =  get_prices_for_sorted_by_date_desc(Ticker ,StartingDate, Window_of_analysis),
    log4erl:debug("Prices for Ticker ~s : ~p", [Ticker, O]),
    [First|Rest] = O,
    {check_price(Rest, First#price.close, First#price.volume), First#price.close, First#price.volume}
.

get_prices_for_sorted_by_date_desc(Ticker, StartingDate, Window_of_analysis) ->
    {atomic,R} = mnesia:transaction(fun() -> 
                               Q = qlc:q([M || M <- mnesia:table(price), M#price.symbol==Ticker, M#price.date >= (StartingDate - Window_of_analysis), M#price.date =< StartingDate]),
                               OrderFun = fun(A,B) ->
                                                  A#price.date > B#price.date
                                          end,
                               Query = qlc:sort(Q, [{order, OrderFun}]),
                               C     = qlc:cursor(Query),
                               R     = qlc:next_answers(C, all_remaining),
                               qlc:delete_cursor(C),
                               R
                       end),
    R
.

check_price([], _,_) -> true;
check_price([H|T], SentinelPrice, SentinelVolume) ->
    case ((H#price.close =< SentinelPrice) and (H#price.volume =< SentinelVolume)) of
        true  -> check_price(T, H#price.close, H#price.volume);
        false -> false
    end
.

