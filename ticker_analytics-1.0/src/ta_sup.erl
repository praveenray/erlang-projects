%%%-------------------------------------------------------------------
%%% File    : ta_sup.erl
%%% Author  : Praveen <daisy@kitchen-laptop>
%%% Description : 
%%%
%%% Created : 25 Jun 2009 by Praveen <daisy@kitchen-laptop>
%%%-------------------------------------------------------------------
-module(ta_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start_link() -> {ok,Pid} | ignore | {error,Error}
%% Description: Starts the supervisor
%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================
%%--------------------------------------------------------------------
%% Func: init(Args) -> {ok,  {SupFlags,  [ChildSpec]}} |
%%                     ignore                          |
%%                     {error, Reason}
%% Description: Whenever a supervisor is started using 
%% supervisor:start_link/[2,3], this function is called by the new process 
%% to find out about restart strategy, maximum restart frequency and child 
%% specifications.
%%--------------------------------------------------------------------
init([]) ->
    AChild = {'AnalyticsServer',{ta_analytics_server,start_link,[]},
	      permanent,3000,worker,[ta_analytics_server]},
    {ok,{{one_for_one,5,10}, [AChild]}}.

%%====================================================================
%% Internal functions
%%====================================================================
