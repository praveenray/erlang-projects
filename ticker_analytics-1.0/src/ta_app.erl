%%%-------------------------------------------------------------------
%%% File    : ta_app.erl
%%% Author  : Daisy <daisy@kitchen-laptop>
%%% Description : 
%%%
%%% Created : 25 Jun 2009 by Daisy <daisy@kitchen-laptop>
%%%-------------------------------------------------------------------
-module(ta_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1, start_phase/3]).

%%====================================================================
%% Application callbacks
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start(Type, StartArgs) -> {ok, Pid} |
%%                                     {ok, Pid, State} |
%%                                     {error, Reason}
%% Description: This function is called whenever an application 
%% is started using application:start/1,2, and should start the processes
%% of the application. If the application is structured according to the
%% OTP design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%%--------------------------------------------------------------------
start(_Type, _StartArgs) ->
    case ta_sup:start_link() of
	{ok, Pid} -> 
	    {ok, Pid};
	Error ->
	    Error
    end.

%%--------------------------------------------------------------------
%% Function: stop(State) -> void()
%% Description: This function is called whenever an application
%% has stopped. It is intended to be the opposite of Module:start/2 and
%% should do any necessary cleaning up. The return value is ignored. 
%%--------------------------------------------------------------------
stop(_State) ->
    ok.

start_phase(log4erl, _, {conf_file, RelativePath}) ->
    {ok, WorkingDir} = application:get_env(base_dir),
    FullConfPath = filename:join(WorkingDir, RelativePath),
    io:format("FullConfPath: ~s~n", [FullConfPath]),
    application:load(log4erl),
    application:start(log4erl),
    log4erl:conf(FullConfPath),
    io:fwrite("log4conf loaded"),
    log4erl:debug("Log4erl Initialized"),
    ok;
start_phase(mnesia,_,{mnesia_dir, _DirPath}) ->
    %application:load(mnesia),
    %application:set_env(mnesia, dir, DirPath),
    %io:format("Set MnesiaDir to : ~s~n", [DirPath]),
    application:start(mnesia),
    mnesia:wait_for_tables([price], infinity);

start_phase(sasl, _, _) ->
    application:start(sasl)
.

%%====================================================================
%% Internal functions
%%====================================================================
