%%%-------------------------------------------------------------------
%%% File    : ta_analytics_server.erl
%%% Author  : Praveen <praveen@lenovo>
%%% Description : 
%%%
%%% Created : 25 Jun 2009 by Praveen <praveen@lenovo>
%%%-------------------------------------------------------------------
-module(ta_analytics_server).

-behaviour(gen_server).
-include_lib("stdlib/include/qlc.hrl").
-include("../../ticker_database-1.0/include/td_includes.hrl").

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-record(state, {}).

%%====================================================================
%% API
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start_link() -> {ok,Pid} | ignore | {error,Error}
%% Description: Starts the server
%%--------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%====================================================================
%% gen_server callbacks
%%====================================================================

%%--------------------------------------------------------------------
%% Function: init(Args) -> {ok, State} |
%%                         {ok, State, Timeout} |
%%                         ignore               |
%%                         {stop, Reason}
%% Description: Initiates the server
%%--------------------------------------------------------------------
init([]) ->
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% Function: %% handle_call(Request, From, State) -> {reply, Reply, State} |
%%                                      {reply, Reply, State, Timeout} |
%%                                      {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, Reply, State} |
%%                                      {stop, Reason, State}
%% Description: Handling call messages
%%--------------------------------------------------------------------
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% Function: handle_cast(Msg, State) -> {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, State}
%% Description: Handling cast messages
%%--------------------------------------------------------------------
handle_cast({run_price_volume_upswings, {StartY, StartM, StartD}, Window_of_analysis} , State) ->
    run_price_volume_upswings({{StartY, StartM, StartD}, Window_of_analysis}),
    {noreply,State}
.

run_price_volume_upswings({{StartY, StartM, StartD}, Window_of_analysis}) ->
    {ok,Analytics_node_count} = application:get_env(ticker_analytics, how_many_processes),
    Active_tickers_list       = get_active_tickers(),
    Tickers_per_node          = length(Active_tickers_list) div Analytics_node_count,
    Rest                      = length(Active_tickers_list) rem Analytics_node_count,

    mnesia:clear_table(price_volume_upswing),
    log4erl:debug("Dispatching ~p Tickers to ~p Children",[Tickers_per_node, Analytics_node_count]),
    dispatch_to_analytics_node(lists:seq(1,Analytics_node_count) , Active_tickers_list,  {StartY,StartM,StartD}, 
                               1, Tickers_per_node, Window_of_analysis),
    dispatch_to_analytics_node([1], Active_tickers_list, {StartY, StartM, StartD},  
                               (Tickers_per_node * Analytics_node_count) + 1,
                               Rest,
                               Window_of_analysis
                              )
.

dispatch_to_analytics_node([], _, _,_,_,_) -> ok;
dispatch_to_analytics_node([_Node_seq | Rest], Active_tickers_list, {Y,M,D}, Start_ticker, Tickers_per_node, Window_of_analysis) -> 
    spawn_link(ta_price_volume_upswings, run, [{Y,M,D},  lists:sublist(Active_tickers_list, Start_ticker, Tickers_per_node), Window_of_analysis]),
    dispatch_to_analytics_node(Rest, Active_tickers_list, {Y,M,D}, Start_ticker + Tickers_per_node, Tickers_per_node, Window_of_analysis)
.

get_active_tickers() ->
    {atomic,List} = mnesia:transaction(fun() ->
                               Q = qlc:q([M#ticker.symbol || M <- mnesia:table(ticker), M#ticker.active == "Y"]),
                               C = qlc:cursor(Q),
                               L = qlc:next_answers(C, all_remaining),
                               qlc:delete_cursor(C),
                               L
                       end),
    List
.

%%--------------------------------------------------------------------
%% Function: handle_info(Info, State) -> {noreply, State} |
%%                                       {noreply, State, Timeout} |
%%                                       {stop, Reason, State}
%% Description: Handling all non call/cast messages
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% Function: terminate(Reason, State) -> void()
%% Description: This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any necessary
%% cleaning up. When it returns, the gen_server terminates with Reason.
%% The return value is ignored.
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% Func: code_change(OldVsn, State, Extra) -> {ok, NewState}
%% Description: Convert process state when code is changed
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%%% Internal functions
%%--------------------------------------------------------------------
