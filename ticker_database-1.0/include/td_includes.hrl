-record(ticker, {
          symbol,
          name,
          exchange,
          active = "Y"
         }).
-record(price, {
          id,
          symbol,
          date,
          open = 0.0,
          close = 0.0,
          mean = 0.0,
          high = 0.0,
          low = 0.0,
          volume = 0.0
         }).

-record(price_ram, {
          id,
          symbol,
          date,
          open = 0.0,
          close = 0.0,
          mean = 0.0,
          high = 0.0,
          low = 0.0,
          volume = 0.0
         }).

-record(price_run, {
          id,
           date,
          status = "idle",
          number_of_tickers = 0,
          start_time = 0,
          end_time = 0
}).

-record(price_volume_upswing, {
          symbol,
          date,
          last_price,
          last_volume
}).
