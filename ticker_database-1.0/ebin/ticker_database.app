{application, ticker_database,
 [{description, "Ticker Database"},
  {vsn, "1.0"},
  {modules, [td_sup, td_cron, td_app,td_create_schema]},
  {registered,[ticker_database]},
  {applications,[kernel,stdlib]},
  {env, [
    {base_dir, "/home/praveen/erlang-projects/ticker_database-1.0"},
    {amex_symbols_file,  "amex-symbols.csv"},
    {nasdaq_symbols_file,"nasdaq-symbols.csv"},
    {nyse_symbols_file,  "nyse-symbols.csv"},
    {price_fetch_delay,  7},
    {java_server, {csvserver,'csvparser@ve-ruby-projects'}},
    {java_server_timeout, 10000},
    {analytics_nodes,['ticker_analytics@ve-ruby-projects']}
  ]},
  {mod,{td_app,[]}},
  {start_phases, [
    {sasl, []},
    {ibrowse,[]},
    {log4erl,{conf_file, "ebin/log4erl.conf"}},
    {mnesia, {mnesia_dir, "/home/praveen/erlang-projects/ticker_database-1.0/Mnesia.ticker_database"}}
  ]}

 ]}.
