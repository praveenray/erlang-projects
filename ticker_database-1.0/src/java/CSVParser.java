import com.ericsson.otp.erlang.*;
import java.io.*;
import java.util.*;
import org.apache.log4j.*;


public class CSVParser {
    protected static Logger logger = null;

    public static void main(String[] args) throws Exception {
        CSVParser.setup_logger();
        logger = Logger.getLogger("CsvReader");

        OtpNode node  = new OtpNode("csvparser", "TICKERDATABASE");
        OtpMbox mbox  = node.createMbox("csvserver");

        while(true) {
            try {
                OtpErlangObject o = mbox.receive();
                logger.debug("Got Message " + o);
                CSVParser.process_request((OtpErlangTuple) o, mbox);
            } catch(Exception exp) {
                logger.error("Exception: " + exp);
            }
        }
    }

    public static void process_request(OtpErlangTuple msg, OtpMbox mbox) throws Exception {
        String request  = ((OtpErlangAtom)(msg.elementAt(0))).atomValue();
        if(request.equals("file_parse"))
            CSVParser.process_csv_file(msg, mbox);
        else if(request.equals("guid"))
            CSVParser.generate_guid(msg,mbox);        
    }
    
    public static void process_csv_file(OtpErlangTuple msg, OtpMbox mbox) throws Exception {
        OtpErlangPid from = (OtpErlangPid)(msg.elementAt(1));
        String file_path  = ((OtpErlangString) msg.elementAt(2)).stringValue();
        String file_type  = ((OtpErlangString) msg.elementAt(3)).stringValue();
        CSVParser.logger.debug("FILEPATH: " + file_path + " File Type: " + file_type);
        List lines = CSVParser.process_file(file_type, file_path);
        OtpErlangObject[] reply = CSVParser._create_reply();
        reply[1]   = new OtpErlangList((OtpErlangObject []) lines.toArray(new OtpErlangList[] {} ));
        CSVParser.logger.debug("Ready to reply " + reply);
        CSVParser._send_reply(mbox,reply,from);
    }    

    public static List process_file(String type, String file_path) throws Exception {
        Map options = new HashMap();
        if(type.equals("price"))
            options.put("uuid", new Boolean(true));
        
        return CSVParser.parse_file(file_path, options);
    }

    public static List parse_file(String file_path, Map options) throws Exception {        
        List lines = new ArrayList();

        com.csvreader.CsvReader reader = new com.csvreader.CsvReader(file_path, ',');
        while(reader.readRecord()) {
            String[] words = reader.getValues();
            int row_len = words.length;
            if(options.get("uuid") != null) 
                row_len++;
            OtpErlangList[] cells = new OtpErlangList[row_len];
            for(int i=0; i < words.length; i++) {
                cells[i] = new OtpErlangList(words[i]);
            }
            if(options.get("uuid") != null) 
                cells[row_len-1] = new OtpErlangList(java.util.UUID.randomUUID().toString());
            OtpErlangList row = new OtpErlangList((OtpErlangObject []) cells);
            lines.add(row);
        }
        CSVParser.logger.debug("Sending back lines " + lines.size());
        return lines;
    }

    public static void generate_guid(OtpErlangTuple msg, OtpMbox mbox) {
        OtpErlangPid from       = (OtpErlangPid)(msg.elementAt(1));
        OtpErlangObject[] reply = CSVParser._create_reply();
        reply[1] = new OtpErlangString(java.util.UUID.randomUUID().toString());
        CSVParser._send_reply(mbox, reply, from);
    }

    public static void setup_logger() throws Exception {
        PropertyConfigurator.configure("log4j.properties");
    }

    public static OtpErlangObject[] _create_reply() {
        OtpErlangObject[] reply = new OtpErlangObject[2];
        reply[0]   = new OtpErlangAtom("ok"); 
        return reply;
    }

    public static void _send_reply(OtpMbox mbox, OtpErlangObject[] reply, OtpErlangPid to) {
        OtpErlangTuple tuple = new OtpErlangTuple(reply);
        mbox.send(to, tuple);        
    }
}

//  {csvserver,'csvparser@ve-ruby-projects'} ! {self(), "/home/praveen/erlang-projects/amex-symbols.csv"}.
