-module(td_create_schema).
-include("../include/td_includes.hrl").
-export([load_tickers/2, 
         load_incremental_prices/1,
         load_all_prices/2, 
         load_ticker_prices/3, 
         load_prices/5, 
         clear_prices/0, 
         create_database/0, create_tables/0, drop_tables/0, 
         to_float/1
        ]).
-define(PRICE_RUN_ID, "PRICE_RUN_ID").

load_tickers(Exchanges_to_do, FromPid) ->
    Exchanges = get_exchages_list(Exchanges_to_do,[]),
    {ok, WorkingDir} = application:get_env(base_dir),
    log4erl:debug("Loading Tickers for ~p, WorkingDir: ~s",[Exchanges, WorkingDir]),
    FilesList = lists:map(
                  fun(Exchange) ->
                          {ok,Value} = application:get_env(list_to_existing_atom(Exchange ++ "_symbols_file")),
                          FullPath = case filename:pathtype(Value) of
                                         absolute -> Value;
                                         relative -> filename:join([WorkingDir, "priv", Value])
                                     end,
                          {Exchange, FullPath}
                  end,
                  Exchanges
    ),
    log4erl:debug("Symbol Files: ~p",[FilesList]),
    {atomic, Tickers_loaded} = mnesia:transaction(fun() ->
                                            load_tickers_with_java(FilesList, 0)
                                    end),
    FromPid ! {load_tickers_done, Tickers_loaded}
.

load_incremental_prices(FromPid) ->
    Run      = hd(mnesia:dirty_read({price_run, ?PRICE_RUN_ID})),
    "idle"   = Run#price_run.status,

    LastRunDate = Run#price_run.date,
    Today    = calendar:date_to_gregorian_days(erlang:date()),
    true     = (Today > LastRunDate),

    mnesia:dirty_write(Run#price_run{
                         status     = "running",
                         start_time = calendar:datetime_to_gregorian_seconds(erlang:universaltime()),
                         end_time   = null
    }),
    {StartYear, StartMonth, StartDay} = calendar:gregorian_days_to_date(LastRunDate+1),
    {TodayY, TodayM, TodayD}          = erlang:date(),
    {ok, "looping"} = load_prices(prepare_price_url({StartYear, StartMonth, StartDay}, {TodayY, TodayM, TodayD}), 
                     mnesia:dirty_all_keys(ticker), LastRunDate, 0, FromPid)
.

load_all_prices({StartYear, StartMonth, StartDay}, FromPid) ->
    Run    = hd(mnesia:dirty_read({price_run, ?PRICE_RUN_ID})),
    "idle" = Run#price_run.status,
    {TodayY, TodayM, TodayD} = erlang:date(), % calendar:gregorian_days_to_date(calendar:date_to_gregorian_days(erlang:date())),
    mnesia:dirty_write(Run#price_run{
                         status     = "running",
                         start_time = calendar:datetime_to_gregorian_seconds(erlang:universaltime()),
                         end_time   = null
    }),
    clear_prices(),
    {ok, "looping"} = load_prices(prepare_price_url({StartYear, StartMonth, StartDay}, {TodayY, TodayM, TodayD}), 
                     mnesia:dirty_all_keys(ticker), null, 0, FromPid)
.

load_ticker_prices(Ticker,{Y,M,D}, FromPid) ->
    Run    = hd(mnesia:dirty_read({price_run, ?PRICE_RUN_ID})),
    "idle" = Run#price_run.status,
    
    {ok, "looping"} = load_prices(prepare_price_url({Y,M,D},{Y,M,D}), [Ticker], null, 0, FromPid)
.

load_prices(_Url,[], MaxDate, TickerCount, FromPid) ->  
    {atomic,_} = mnesia:transaction(fun() ->
                               Run = hd(mnesia:read({price_run, ?PRICE_RUN_ID})),
                               case Run#price_run.status of
                                   "running" -> 
                                       mnesia:write(Run#price_run{
                                                      status   = "idle",
                                                      date     = MaxDate,
                                                      number_of_tickers = TickerCount,
                                                      end_time = calendar:datetime_to_gregorian_seconds(erlang:universaltime())
                                                     }),
                                       ok;
                                   _ -> ok
                               end
                       end
                      ),

    NewMaxDate = case MaxDate of
                     null -> null;
                     _    -> calendar:gregorian_days_to_date(MaxDate)
                 end,

    case FromPid of 
        null -> ok;
        _    -> FromPid ! {load_prices_done, NewMaxDate, TickerCount}
    end,
    {ok,MaxDate,TickerCount};

load_prices(YahooUrl,  [Ticker|Tickers], MaxDate, TickerCount, FromPid) ->
    log4erl:debug("Fetching ~s~n", [Ticker]),
    Url  = re:replace(YahooUrl, "TICKER", Ticker, [{return,list}]),
    
    Resp = ibrowse:send_req(Url, [], get, [], 
                            [{save_response_to_file, true}],
                            5000),

    {ok, NewMaxDate, NewTickerCount} = case Resp of 
                           {ok, "200", _Headers, {file, FileName}} -> 
                               {ok,Java_server} = application:get_env(ticker_database, java_server),
                               Java_server ! {file_parse, self(), FileName, "price" },
                               receive
                                   {ok, Lines} -> 
                                       file:delete(FileName),
                                       [_Header|Rest] = Lines,
                                       {ok, Date} = insert_prices(Ticker, Rest, MaxDate),
                                       {ok, Date, TickerCount+1};
                                   R ->
                                       log4erl:warn("Java Server Sent back bad Response ~p",[R]),
                                       {ok, MaxDate, TickerCount}
                               end;
                           _ -> log4erl:info("Price Fetch for ~s Failed~n",[Ticker]),
                                {ok, MaxDate, TickerCount}
                       end,

    {ok,Delay} = application:get_env(ticker_database, price_fetch_delay),
    timer:apply_after(Delay, ?MODULE, load_prices, [YahooUrl, Tickers, NewMaxDate, NewTickerCount, FromPid]),
    {ok, "looping"}
.

prepare_price_url({StartY, StartM, StartD},{EndY, EndM, EndD}) ->
    YahooUrl = lists:flatten(io_lib:fwrite(
                               "http://ichart.finance.yahoo.com/table.csv?s=TICKER&a=~.B&b=~.B&c=~.B&d=~.B&e=~.B&f=~.B&g=d&ignore=.csv",
                               [StartM-1, StartD, StartY,EndM-1,EndD,EndY])
                            ),
    log4erl:debug("YahooUrl: ~s", [YahooUrl]),
    YahooUrl
.    
   
insert_prices(_Ticker,[], MaxDate) -> {ok, MaxDate};
insert_prices(Ticker, [H|T], MaxDate) ->
    [Date,Open,High,Low, _Close,Volume,AdjClose, Uuid] = H,
    %log4erl:debug("Inserting ~p", [H]), 
    
    Rec = #price{
                 id     = Uuid,
                 symbol = string:to_lower(Ticker),
                 date   = to_days(Date),
                 open   = to_float(Open),
                 close  = to_float(AdjClose),
                 high   = to_float(High),
                 low    = to_float(Low),
                 volume = to_int(Volume)
    },

    %log4erl:debug("Rec: ~p", [Rec]),
    %mnesia:transaction(fun() -> mnesia:write(Rec) end),
    {atomic,NewDate} = mnesia:transaction(fun() ->
                               % if ticker+date exists, overwrite it, else create new one
                               ExistingList = mnesia:match_object({price,'$0', Ticker, Rec#price.date, 
                                                                   '_', '_','_','_','_','_'}),
                               case (length(ExistingList) > 0) of
                                   true -> 
                                       ExistingPrice = hd(ExistingList),
                                       Price  = Rec#price{id = ExistingPrice#price.id};
                                   false -> 
                                       Price = Rec,
                                       mnesia:write(Price)
                               end,
                               
                               mnesia:write(Price),

                               case ((MaxDate /= null) and (MaxDate >= Price#price.date)) of
                                   true  -> MaxDate;
                                   false -> Price#price.date
                               end
                       end),
    
    insert_prices(Ticker, T, NewDate)
.

to_float(N) ->
    case string:to_float(N) of
        {error,_} -> 
            log4erl:debug("Error Converting ~s to Float", [N]),
            0.0;
        {Float,_} -> Float
    end
.

to_int(N) ->
    case string:to_integer(N) of
        {error,_} -> 
            log4erl:debug("Error Converting ~s to Integer", [N]),
            0.0;
        {Integer,_} -> Integer
    end
.
    
to_days(S) ->
    [Y,M,D] = lists:map(fun(X) -> 
                      {I,_} = string:to_integer(X),
                      I
              end,
              string:tokens(S, "-")
    ),
    calendar:date_to_gregorian_days(Y,M,D)
.

clear_prices() ->
    mnesia:clear_table(price).

load_tickers_with_java([], Count) -> Count;
load_tickers_with_java([Exchange|Rest], Ticker_count) ->
    {ok, Count} = load_tickers_with_java(Exchange),
    load_tickers_with_java(Rest, Ticker_count + Count).

load_tickers_with_java({Exchange, CsvFile}) ->
    {ok,Java_server}         = application:get_env(ticker_database, java_server),
    {ok,Java_server_timeout} = application:get_env(ticker_database, java_server_timeout),
    Java_server ! {file_parse, self(), CsvFile, "ticker"},
    receive
        {ok, Lines} -> 
            log4erl:debug("~p\n", [erlang:length(Lines)]),
            insert_tickers(Lines, Exchange),
            {ok, length(Lines)};
        _ -> {csv_parse_error, io_lib:format("Failed to Parse CSVFile ~s",[CsvFile])}
        after Java_server_timeout -> {java_server_timeout, io_lib:format("Java Server ~p Timeout",[Java_server])}
    end.

insert_tickers([], _Exchange) -> {ok};
insert_tickers([H|T], Exchange) ->
    {atomic,_} = insert_ticker(H, Exchange),
    insert_tickers(T, Exchange).

insert_ticker([Company_name,Symbol], Exchange) ->
    % log4erl:debug("Inserting: ~s~n", [Symbol]),
    Rec = #ticker{name = Company_name, symbol = string:to_lower(Symbol), exchange = Exchange},
    mnesia:transaction(fun() -> mnesia:write(Rec) end)
.

get_exchages_list([], Exchanges) -> Exchanges;
get_exchages_list([Name|Rest], Exchanges) -> 
    case Name of
        "amex" -> get_exchages_list(Rest, lists:append(Exchanges, ["amex"]));
        "nyse" -> get_exchages_list(Rest, lists:append(Exchanges, ["nyse"]));
        "nasdaq" -> get_exchages_list(Rest, lists:append(Exchanges, ["nasdaq"]))
    end
.    
             
    
                         
create_database() ->
    mnesia:create_schema([node()]).

create_tables() ->
    Nodes = lists:append([node()], nodes()),
    mnesia:create_table(ticker, [{disc_copies, Nodes}, {attributes, record_info(fields, ticker)} ]),
    log4erl:debug("Creating price with ~p", [record_info(fields, price)]),
    mnesia:create_table(price, [{disc_copies, Nodes},{type, set}, {attributes, record_info(fields, price)} ]),
    mnesia:create_table(price_run, [{disc_copies, Nodes},{type, set}, {attributes, record_info(fields, price_run)} ]),
    mnesia:create_table(price_volume_upswing, [{disc_copies, Nodes},{type, set}, {attributes, record_info(fields, price_volume_upswing)} ]),
    mnesia:dirty_write(#price_run{
          id = ?PRICE_RUN_ID,
           date = calendar:date_to_gregorian_days({1990,1,1})
    })
.

drop_tables() ->
    mnesia:delete_table(ticker),
    mnesia:delete_table(price).


%%%
%lists:foreach(fun(X) -> io:format("~p~n",[mnesia:dirty_read({price,X})])  end, mnesia:dirty_all_keys(price)).
%%%
