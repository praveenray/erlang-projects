%%%-------------------------------------------------------------------
%%% File    : td_cron.erl
%%% Author  : Praveen Ray <praveen@lenovo>
%%% Description : 
%%%
%%% Created : 19 Jun 2009 by Praveen Ray <praveen@lenovo>
%%%-------------------------------------------------------------------
-module(td_cron).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3
         ]
       ).

-record(state, 
         {
          from_pid  = null,
          child_pid = null
         }
).

%%====================================================================
%% API
%%====================================================================
%%--------------------------------------------------------------------
%% Function: start_link() -> {ok,Pid} | ignore | {error,Error}
%% Description: Starts the server
%%--------------------------------------------------------------------
start_link() ->
    process_flag(trap_exit, true),
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%====================================================================
%% gen_server callbacks
%%====================================================================

%%--------------------------------------------------------------------
%% Function: init(Args) -> {ok, State} |
%%                         {ok, State, Timeout} |
%%                         ignore               |
%%                         {stop, Reason}
%% Description: Initiates the server
%%--------------------------------------------------------------------
init([]) ->
    LoopData = #state{},
    {ok, LoopData}
.


handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.
    
%%--------------------------------------------------------------------
%% Function: handle_cast(Msg, State) -> {noreply, State} |
%%                                      {noreply, State, Timeout} |
%%                                      {stop, Reason, State}
%% Description: Handling cast messages
%%--------------------------------------------------------------------

handle_cast(stop, State) ->
    {stop, normal, State};

handle_cast({load_tickers, Exchanges_to_do, FromPid}, State) ->
    Child_pid = spawn_link(td_create_schema, load_tickers, [Exchanges_to_do, FromPid]),
    {noreply, State#state{
                from_pid  = FromPid,
                child_pid = {load_tickers,Child_pid}
               }};

handle_cast({load_all_prices, {FromY,FromM,FromD}, FromPid}, State) ->
    Child_pid = spawn_link(td_create_schema, load_all_prices, [{FromY,FromM,FromD}, FromPid]),
   {noreply, State#state{
                from_pid  = FromPid,
                child_pid = {load_all_prices,Child_pid}
               }};
    
handle_cast({load_prices_incrementally, FromPid}, State) ->
    Child_pid = spawn_link(td_create_schema, load_incremental_prices, [FromPid]),
    {noreply, State#state{child_pid = {load_prices_incrementally, Child_pid},
                          from_pid  = FromPid
                         }};

handle_cast({load_a_price, Ticker, {Y,M,D}, FromPid}, State) ->
    Child_pid = spawn_link(td_create_schema, load_a_price, []),
    {noreply, State#state{
                from_pid  = FromPid,
                child_pid = {load_all_prices,Child_pid}
               }};

handle_cast({'EXIT', Child_pid, Reason}, State) ->
    case State#state.child_pid of
        {load_prices_incrementally, Child_pid} -> 
            gen_server:cast(State#state.from_pid, {load_prices_failed, Reason});
        {load_tickers, Child_pid} ->
            gen_server:cast(State#state.from_pid, {load_tickers_failed, Reason});
        {load_all_prices, Child_pid} ->
            gen_server:cast(State#state.from_pid, {load_all_prices, Reason})
    end
.

%%--------------------------------------------------------------------
%% Function: handle_info(Info, State) -> {noreply, State} |
%%                                       {noreply, State, Timeout} |
%%                                       {stop, Reason, State}
%% Description: Handling all non call/cast messages
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% Function: terminate(Reason, State) -> void()
%% Description: This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any necessary
%% cleaning up. When it returns, the gen_server terminates with Reason.
%% The return value is ignored.
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% Func: code_change(OldVsn, State, Extra) -> {ok, NewState}
%% Description: Convert process state when code is changed
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%%% Internal functions
%%--------------------------------------------------------------------
